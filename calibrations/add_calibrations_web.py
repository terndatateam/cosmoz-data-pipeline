from calibration_utils import get_calibration_records_from_csv, get_calibration_files_from_directory
from requests import Response, post

import argparse
import simplejson as json


def add_station_calibrations_web(base_url: str, api_key: str, site_no: int, file_path: str) -> None:
    if api_key is None or api_key == "":
        raise ValueError("The provided API key cannot be None or an empty string.")
    
    target_url = f"{base_url}/calibrations"
    headers = {
        "X-API-Key": api_key
    }

    records = get_calibration_records_from_csv(file_path)

    if len(records) < 1:
        raise RuntimeError(f"Cannot find any calibration records in the specified file: {file_path}.")

    count = 0
    for record in records:
        payload = {
            "calibration": record
        }

        json_payload = json.dumps(payload, use_decimal=True, default=str)

        res: Response = post(url=target_url, data=json_payload, headers=headers)

        if res.status_code == 200:
            count += 1
        else:
            print(f"Failed to upload calibration for calibration no.: {record['calibration_no']}")
            print(f"Status Code: {res.status_code}")
            print(res.json())

    print(f"{count} records uploaded for site: '{site_no}', from file: '{file_path}'")


def main():
    parser = argparse.ArgumentParser(description="Standalone script for importing CosmOz site calibrations from local CSV files via the CosmOz REST API creation endpoints.")
    parser.add_argument(
        "-d",
        "--directory",
        dest="directory",
        help="The directory where the target file/s are located.",
        type=str,
        default="./in_files"
    )
    parser.add_argument(
        "--web-api-key",
        dest="web_api_key",
        help="The API key to be used to authenticate outgoing creation requests.",
        type=str,
        required=True
    )
    parser.add_argument(
        "--web-base-url",
        dest="web_base_url",
        help="The base URL of the target CosmOz REST API.",
        type=str,
        default="http://localhost:9001/rest"
    )

    args = parser.parse_args()
    files = get_calibration_files_from_directory(args.directory)

    for site_no, file in files:
        try:
            print(f"Starting uploads for site: '{site_no}', from file: '{file}'")
            add_station_calibrations_web(args.web_base_url, args.web_api_key, site_no, file)
        except RuntimeError as r:
            print(r)


if __name__ == "__main__":
    main()
