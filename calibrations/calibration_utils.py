from bson import Decimal128
from datetime import timezone, datetime
from typing import List, Tuple, Dict, Any
from glob import glob

import csv
import os


def get_calibration_files_from_directory(dir: str) -> List[Tuple[int, str]]:
    """
    This function identifies all CosmOz calibration CSV files in the given directory and returns a list of tuples 
    containing the each file's 'site_no' and relative filepath. 
    """
    calibration_files = glob(f'{dir}/Cal_Input_*.csv')

    if len(calibration_files) < 1:
        raise RuntimeError(f"No input files could be found at the specified directory: '{dir}', matching the 'Cal_Input_*.csv' format.")
    
    out = []
    for cf in calibration_files:
        # Strip out the file name for site_no extraction
        file_name = os.path.basename(cf)
        (_, _, _site_name, _site_no) = str(file_name)[:-4].split('_', 4)

        try:
            site_no = int(_site_no)
        except ValueError:
            print(f'Bad calibration file name or site number: {cf}')

        out.append((site_no, cf))

    return out


def get_calibration_records_from_csv(file_path: str) -> List[Dict[str, Any]]:
    """
    This function takes a target CosmOz calibration CSV file and maps each row to the calibration database model,
    returning a list of records ready for persistence.
    """
    with open(file_path, 'r') as input_file:
        reader = csv.DictReader(input_file)
        records = []

        for row in reader:
            try:
                site_no = row['site_number']
                
                record = {
                    'calibration_no': int(row['calibration_no']),
                    'date_UTC': datetime.strptime(row['date_UTC'], '%d/%m/%Y').replace(
                        tzinfo=timezone.utc
                    ),
                    'site_number': site_no,
                    'latitude': Decimal128(row['latitude']),
                    'longitude': Decimal128(row['longitude']),
                    'elevation_m': int(row['altitude_m']),  # Elevation is the more correct term to be used here as requested. However the column name in input file is unchanged at this time.
                    'cutoff_rig': Decimal128(row['cutoff_rig']),
                    'sample_location': int(row['sample_location']),
                    'sample_dir': row['sample_dir'],
                    'upper_depth_cm': int(row['upper_depth_cm']),
                    'lower_depth_cm': int(row['lower_depth_cm']),
                    'distance_from_CRNS_m': int(row['distance_from_CRNS_m']),
                    'depth_interval': row['depth_interval'],
                    'gravimetric_soil_moisture': Decimal128(row['gravimetric_soil_moisture']),
                    'bulk_density_gcm3': Decimal128(row['bulk_density_g/cm3']),
                    'volumetric_soil_moisture': Decimal128(row['volumetric_soil_moisture']),
                    'lattice_g_g': Decimal128(row['lattice_g_g']),
                    'organicC_g_g': Decimal128(row['organicC_g_g']),
                    'cal_start_UTC': datetime.strptime(row['cal_start_UTC'], "%d/%m/%Y %H:%M").replace(
                        tzinfo=timezone.utc
                    ),
                    'cal_end_UTC': datetime.strptime(row['cal_end_UTC'], "%d/%m/%Y %H:%M").replace(
                        tzinfo=timezone.utc
                    ),
                    'ave_counts_cph': Decimal128(row['ave_counts_cph']),
                    'ave_pressure_mb': Decimal128(row['ave_pressure_mb']),
                    'ave_temp_oC': Decimal128(row['ave_temp_oC']),
                    'ave_rel_humidity_pc': Decimal128(row['ave_rel_humidity_pc']),
                    'neutron_monitor': row['neutron_monitor'],
                    'reference_intensity': Decimal128(row['reference_intensity']),
                    'neutron_intensity': Decimal128(row['neutron_intensity']),
                }
                records.append(record)
            except Exception as e:
                print(e)
                print(f'Incompatible field formatting in row: {reader.line_num}. Please correct this field and retry.')

    return records
    