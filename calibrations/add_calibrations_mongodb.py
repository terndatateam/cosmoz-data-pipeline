from pymongo import MongoClient, UpdateOne
from calibration_utils import get_calibration_records_from_csv, get_calibration_files_from_directory

import argparse


def add_station_calibrations_db(client: MongoClient, db_name: str, site_no: int, file_path: str) -> None:
    db = getattr(client, db_name)
    all_stations_collection = db.all_stations
    cal_collection = db.stations_calibration_v2

    # Check if target site exists in collection
    res = all_stations_collection.find({"site_no": site_no})
    sites = []

    for i in res:
        _ = i["site_no"]
        sites.append(i)

    if len(sites) < 1:
        raise RuntimeError(f"Cannot find site {site_no} to add calibration records to.")
    
    records = get_calibration_records_from_csv(file_path)

    if len(records) < 1:
        raise RuntimeError(f"Cannot find any calibration records in the specified file: {file_path}")

    count = 0
    upserts = []
    for r in records:
        upserts.append(
            UpdateOne(
                filter={
                    'site_number': site_no, 
                    'calibration_no': r['calibration_no'], 
                    'sample_location': r['sample_location'], 
                    'upper_depth_cm': r['upper_depth_cm']},
                update={'$set': r},
                upsert=True,
            )
        )
        count += 1

    cal_collection.bulk_write(upserts, ordered=False)
    print(f"{count} records written/updated for site: '{site_no}', from file: '{file_path}'")


def main():
    parser = argparse.ArgumentParser(description="Standalone script for importing CosmOz site calibrations from local CSV files via a direct MongoDB connection.")
    
    parser.add_argument(
        '-d',
        '--directory',
        dest="directory",
        help='The directory where the target file/s are located.',
        type=str,
        default="./in_files"
    )
    parser.add_argument(
        '--db-port',
        dest="db_port",
        help='The port number of the MongoDB database to connect to.',
        type=int,
        default=27017
    )
    parser.add_argument(
        '--db-host',
        dest="db_host",
        help='The hostname of the MongoDB database to connect to.',
        type=str,
        default="localhost" 
    )
    parser.add_argument(
        '--db-name',
        dest="db_name",
        help='The name of the MongoDB database to connect to.',
        type=str,
        default="cosmoz"
    )

    args = parser.parse_args()

    client = MongoClient(args.db_host, args.db_port)
    files = get_calibration_files_from_directory(args.directory)

    for site_no, file in files:
        try:
            print(f"Starting database upload for site: '{site_no}', from file: '{file}'")
            add_station_calibrations_db(client, args.db_name, site_no, file)
        except RuntimeError as r:
            print(r)


if __name__ == "__main__":
    main()
