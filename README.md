This is the code repository that contains code responsible for running the Cosmoz Soil Moisture algorithm.

The main code is in the `pipeline` directory, its job is to take Raw Data from the InfluxDb and process it to Level1
data, Level1 to Level2, Level2 to Level3, and Level3 to Level4.

The `pipeline` directory is implemented as a python "module", or a library that must be imported before it can be used.

That is the purpose of `cosmoz_process_levels.py`, it imports `pipeline` module and runs it.

To create a dev docker image to use as a coding environment:

```shell
#This uses docker build to build the container and tag it
cd contrib/docker
./build_dev.sh
```

or

```shell
#This uses docker-compose build to build the container in the compose environment
cd contrib/docker
docker compose --env-file ./.env -f ./docker-compose.build.yml -p landscapes-cosmoz-pipeline build cosmoz.pipeline.application-dev
```

To execute the `cosmoz_process_levels.py` script inside the dev container:
```shell
cd contrib/docker
docker compose --env-file ./.env -f ./docker-compose.dev.yml -p landscapes-cosmoz-pipeline run cosmoz.pipeline.application-host poetry run python3 cosmoz_process_levels.py
```

