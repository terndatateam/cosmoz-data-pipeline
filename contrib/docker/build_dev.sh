#!/bin/sh
exec env DOCKER_BUILDKIT=1 docker build -t docker.io/library/pipeline-dev:latest --network=host -f ./Dockerfiles/pipeline-dev.Dockerfile ../../
