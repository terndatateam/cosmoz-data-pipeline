FROM alpine:3.18.5
MAINTAINER Ashley Sommer <Ashley.Sommer@csiro.au>
LABEL org.opencontainers.image.base.name="alpine"
LABEL org.opencontainers.image.base.digest="sha256:d695c3de6fcd8cfe3a6222b0358425d40adfd129a8a47c3416faff1a8aece389"
LABEL maintainer="Ashley.Sommer@csiro.au"
RUN apk add --no-cache --update ca-certificates bash tini-static python3 py3-pip py3-wheel libuv libstdc++ gcompat openssl curl
RUN apk add --no-cache --update --virtual buildenv git libuv-dev libffi-dev python3-dev openssl-dev py3-cffi build-base
RUN pip3 install --upgrade "pip" "wheel"
RUN pip3 install --upgrade --ignore-installed "distlib" "virtualenv" "cryptography>=41.0.5" "poetry>=1.7.1,<2"
# Add CSIRO internal Certs
RUN curl -ks http://certificates.csiro.au/cert/CSIRO-Internal-Root.pem -o '/usr/local/share/ca-certificates/CSIRO-Internal-Root.crt'
RUN curl -ks http://certificates.csiro.au/cert/CSIRO-Internal-CA1.pem -o '/usr/local/share/ca-certificates/CSIRO-Internal-CA1.crt'
RUN update-ca-certificates
# pycharm needs /tmp to be writable
RUN chmod -R 777 /tmp
WORKDIR /usr/local/lib/cosmoz-data-pipeline
ADD . .
RUN chmod -R 777 .
RUN addgroup -g 1000 -S cosmoz &&\
    adduser --disabled-password --gecos "" --home "$(pwd)" --ingroup "cosmoz" --no-create-home --uid 1000 cosmoz
USER cosmoz
RUN python3 -m virtualenv -p /usr/bin/python3 --system-site-packages .venv
RUN . ./.venv/bin/activate && \
    poetry config virtualenvs.in-project true &&\
    poetry config virtualenvs.options.system-site-packages true &&\
    poetry config virtualenvs.prefer-active-python true
RUN . ./.venv/bin/activate &&\
    poetry run pip3 install --upgrade cython &&\
    poetry install -v --no-dev --no-root &&\
    deactivate
USER root
RUN apk del buildenv
USER cosmoz
ENV MONGODB_HOST=localhost
ENV MONGODB_PORT=27017
ENV INFLUXDB_HOST=localhost
ENV INFLUXDB_PORT=8086
ENTRYPOINT ["/sbin/tini-static", "--"]
# By default this container does nothing
CMD ["tail","-f","/dev/null"]
