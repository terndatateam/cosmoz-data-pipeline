FROM alpine:3.17.5
MAINTAINER Ashley Sommer <Ashley.Sommer@csiro.au>
LABEL org.opencontainers.image.base.name="alpine"
LABEL org.opencontainers.image.base.digest="sha256:7298bd41b8aee1adde2a0a2819f695b5057bfd0234bf250f0e5d9946c7ab38bd"
LABEL maintainer="Ashley.Sommer@csiro.au"
RUN echo "https://dl-cdn.alpinelinux.org/alpine/v3.17/main" >> /etc/apk/repositories
RUN echo "https://dl-cdn.alpinelinux.org/alpine/v3.17/community" >> /etc/apk/repositories
RUN apk add --no-cache --update ca-certificates bash tini-static python3 py3-pip py3-wheel libuv libstdc++ gcompat freetds openssl curl
RUN apk add --no-cache --update --virtual buildenv git libuv-dev libffi-dev freetds-dev python3-dev openssl-dev py3-cffi build-base
RUN pip3 install --upgrade "pip" "wheel"
RUN pip3 install --upgrade --ignore-installed "distlib" "virtualenv" "cryptography>=41.0.5" "poetry>=1.7.0,<2"
# Add CSIRO internal Certs
RUN curl -ks http://certificates.csiro.au/cert/CSIRO-Internal-Root.pem -o '/usr/local/share/ca-certificates/CSIRO-Internal-Root.crt'
RUN curl -ks http://certificates.csiro.au/cert/CSIRO-Internal-CA1.pem -o '/usr/local/share/ca-certificates/CSIRO-Internal-CA1.crt'
RUN update-ca-certificates
WORKDIR /usr/local/lib
ARG CLONE_BRANCH=master
ARG CLONE_ORIGIN="https://bitbucket.org/terndatateam/cosmoz-data-pipeline"
ARG CLONE_COMMIT=HEAD
RUN git clone --branch "${CLONE_BRANCH}" "${CLONE_ORIGIN}" src && mv ./src ./cosmoz-data-pipeline
WORKDIR /usr/local/lib/cosmoz-data-pipeline
RUN git checkout "${CLONE_COMMIT}"
RUN chmod -R 777 .
RUN addgroup -g 1000 -S cosmoz &&\
    adduser --disabled-password --gecos "" --home "$(pwd)" --ingroup "cosmoz" --no-create-home --uid 1000 cosmoz
USER cosmoz
RUN python3 -m virtualenv -p /usr/bin/python3 --system-site-packages .venv
RUN . ./.venv/bin/activate && \
    poetry config virtualenvs.in-project true &&\
    poetry config virtualenvs.options.system-site-packages true &&\
    poetry config virtualenvs.prefer-active-python true
RUN . ./.venv/bin/activate &&\
    poetry run pip3 install --upgrade cython &&\
    poetry install -v --no-dev --no-root &&\
    deactivate
USER root
RUN apk del buildenv
USER cosmoz
ENV MONGODB_HOST=localhost
ENV MONGODB_PORT=27017
ENV INFLUXDB_HOST=localhost
ENV INFLUXDB_PORT=8086
ENTRYPOINT ["/sbin/tini-static", "--"]
# By default this container does nothing
CMD ["tail","-f","/dev/null"]
