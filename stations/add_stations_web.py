from station_utils import get_station_information_from_tabular_data, get_station_information_from_excel
from requests import Response, post, get, put
from typing import List

import argparse
import simplejson as json
import os


def get_existing_station_numbers(base_url: str) -> List[int]:
    target_url = f"{base_url}/stations"

    response = get(url=target_url)

    return [station['site_no'] for station in response.json()["stations"]]


def add_station_information_web(base_url: str, api_key: str, file_path: str, target_site_numbers: List[int] = None) -> None:
    if api_key is None or api_key == "":
        raise ValueError("The provided API key cannot be None or an empty string.")

    headers = {
        "X-API-Key": api_key
    }

    file_ext = os.path.splitext(file_path)[1]
    
    if file_ext in ['.tsv', '.csv']:
        stations = get_station_information_from_tabular_data(file_path, target_site_numbers)
    elif file_ext == '.xlsx':
        stations = get_station_information_from_excel(file_path, target_site_numbers)
    else:
        raise RuntimeError(f"The supplied file is not of a supported type (CSV, TSV, XLSX).")

    if len(stations) < 1:
        raise RuntimeError(f"Cannot find any stations in the specified file: {file_path}.")
    
    existing_station_numbers = get_existing_station_numbers(base_url)

    count = 0
    for station in stations:
        payload = {
            "station": station
        }

        json_payload = json.dumps(payload, use_decimal=True, default=str)
        
        target_site_no = station['site_no']
        if target_site_no not in existing_station_numbers:
            target_url = f"{base_url}/stations"
            res: Response = post(url=target_url, data=json_payload, headers=headers)
        else:
            target_url = f"{base_url}/stations/{target_site_no}"
            res: Response = put(url=target_url, data=json_payload, headers=headers)

        if res.status_code == 200:
            count += 1
        else:
            print(f"Failed to upload station: '{station['site_no']}:{station['site_name']}', from file: '{file_path}'")
            print(f"Status Code: {res.status_code}")
            print(res.json())

    print(f"{count} stations uploaded from file: '{file_path}'")


def main():
    parser = argparse.ArgumentParser(description="Standalone script for importing CosmOz sites from a local file via the CosmOz REST API creation endpoints.")
    parser.add_argument(
        "-f",
        "--file-path",
        dest="file_path",
        help="The file path where the target file is located.",
        type=str,
        required=True
    )
    parser.add_argument(
        "--web-api-key",
        dest="web_api_key",
        help="The API key to be used to authenticate outgoing creation requests.",
        type=str,
        required=True
    )
    parser.add_argument(
        "--web-base-url",
        dest="web_base_url",
        help="The base URL of the target CosmOz REST API.",
        type=str,
        default="http://localhost:9001/rest"
    )
    parser.add_argument(
        "-t",
        "--target-site-numbers",
        dest="target_site_numbers",
        help="A comma separated string of site numbers to be imported from the target file. All sites in the document will be imported if this is not specified.",
        type=str,
        default=""
    )

    args = parser.parse_args()
    file_path = args.file_path

    target_sites = args.target_site_numbers

    if target_sites == "":
        target_site_numbers = None
    else:
        try:
            target_sites_split = args.target_site_numbers.split(',')
            target_site_numbers = [int(x) for x in target_sites_split]
        except ValueError as e:
            print("Failed to parse target site numbers. Please check input.")
            print(f"Error: {e}")

    try:
        print(f"Starting upload for sites from file: '{file_path}")
        add_station_information_web(args.web_base_url, args.web_api_key, file_path, target_site_numbers)
    except RuntimeError as r:
        print(r)


if __name__ == "__main__":
    main()
