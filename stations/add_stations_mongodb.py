from pymongo import MongoClient, UpdateOne
from station_utils import get_station_information_from_tabular_data, get_station_information_from_excel
from typing import List

import argparse
import os


def add_station_information_db(client: MongoClient, db_name: str, file_path: str, target_site_numbers: List[int] = None) -> None:
    db = getattr(client, db_name)
    all_stations_collection = db.all_stations

    file_ext = os.path.splitext(file_path)[1]
    
    if file_ext in ['.tsv', '.csv']:
        stations = get_station_information_from_tabular_data(file_path, target_site_numbers)
    elif file_ext == '.xlsx':
        stations = get_station_information_from_excel(file_path, target_site_numbers)
    else:
        raise RuntimeError(f"The supplied file is not of a supported type (CSV, TSV, XLSX).")

    if len(stations) < 1:
        raise RuntimeError(f"Cannot find any calibration records in the specified file: {file_path}")
    
    count = 0
    upserts = []
    for s in stations:
        upserts.append(
            UpdateOne(
                filter={'site_no': s['site_no']},
                update={"$set": s},
                upsert=True
            )
        )
        count += 1

    all_stations_collection.bulk_write(upserts, ordered=False)
    print(f"{count} stations uploaded from file: '{file_path}'")


def main():
    parser = argparse.ArgumentParser(description="Standalone script for importing CosmOz stations from a local file via a direct MongoDB connection.")
    parser.add_argument(
        '-f',
        '--file-path',
        dest="file_path",
        help='The file path where the target file is located.',
        type=str,
        required=True
    )
    parser.add_argument(
        '--db-port',
        dest="db_port",
        help='The port number of the MongoDB database to connect to.',
        type=int,
        default=27017
    )
    parser.add_argument(
        '--db-host',
        dest="db_host",
        help='The hostname of the MongoDB database to connect to.',
        type=str,
        default="localhost" 
    )
    parser.add_argument(
        '--db-name',
        dest="db_name",
        help='The name of the MongoDB database to connect to.',
        type=str,
        default="cosmoz"
    )
    parser.add_argument(
        "-t",
        "--target-site-numbers",
        dest="target_site_numbers",
        help="A comma separated string of site numbers to be imported from the target file. All sites in the document will be imported if this is not specified.",
        type=str,
        default=""
    )
    
    args = parser.parse_args()

    client = MongoClient(args.db_host, args.db_port)
    file_path = args.file_path

    target_sites = args.target_site_numbers

    if target_sites == "":
        target_site_numbers = None
    else:
        try:
            target_sites_split = args.target_site_numbers.split(',')
            target_site_numbers = [int(x) for x in target_sites_split]
        except ValueError as e:
            print("Failed to parse target site numbers. Please check input.")
            print(f"Error: {e}")

    try:
        print(f"Starting database upload for sites from file: {file_path}")
        add_station_information_db(client, args.db_name, file_path, target_site_numbers)
    except RuntimeError as r:
        print(r)


if __name__ == "__main__":
    main()
