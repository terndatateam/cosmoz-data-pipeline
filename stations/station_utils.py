from typing import List, Dict, Any, Tuple
from bson import Decimal128
from ciso8601 import parse_datetime

import csv
import os
import openpyxl


# ----- CSV/TSV READ METHODS -----


def get_station_information_from_tabular_data(file_path: str, target_site_numbers: List[int] = None) -> List[Dict[str, Any]]:
    if file_path is None or file_path == "":
        raise ValueError("File path cannot be null or empty")
    
    file_ext = os.path.splitext(file_path)[1]

    with open(file_path, "r", encoding="latin1") as file:
        if file_ext == ".tsv":
            reader = csv.DictReader(file, delimiter="\t")
        elif file_ext == ".csv":
            reader = csv.DictReader(file)
        else:
            raise ValueError(f"Input file type: '{file_ext}' is not supported. Please use a file in CSV or TSV format instead.")
        
        stations = []

        if target_site_numbers != None:
            filtering = True

        for row in reader:
            try:
                if filtering and int(row['site_no']) not in target_site_numbers:
                    continue
                
                station = {
                    'site_no': int(row['site_no']),
                    'site_name': row['site_name'],
                    'tube_type': row['tube_type'],
                    'network': row['network'],
                    'imei': row['imei'],
                    'sat_data_select': row['sat_data_select'],
                    'hydroinnova_serial_no': row['hydroinnova_serial_no'],
                    'latitude': Decimal128(row['latitude']),
                    'longitude': Decimal128(row['longitude']),
                    'elevation': Decimal128(row['altitude']),  # Elevation is the more correct term to be used here as requested. However the column name in input file is unchanged at this time.
                    'installation_date': parse_datetime(row['installation_date']),
                    'contact': row['contact'],
                    'email': row['email'],
                    'site_description': row['site_description'],
                    'calibration_type': row['calibration_type'],
                    'timezone': str(row['timezone']),
                    'ref_pressure': Decimal128(row['ref_pressure']),
                    'ref_intensity': Decimal128(row['ref_intensity']),
                    'cutoff_rigidity': Decimal128(row['cutoff_rigidity']),
                    'elev_scaling': Decimal128(row['elev_scaling']),
                    'latit_scaling': Decimal128(row['latit_scaling']),
                    'scaling': Decimal128(row['scaling']),
                    'beta': Decimal128(row['beta']),
                    'n0_cal': Decimal128(row['n0_cal']),
                    'bulk_density': Decimal128(row['bulk_density']),
                    'lattice_water_g_g': Decimal128(row['lattice_water_g_g']),
                    'soil_organic_matter_g_g': Decimal128(row['soil_organic_matter_g_g']),
                    'site_photo_name': row['site_photo_name'],
                    'nmdb': row['nmdb'],
                    'tau': row['tau'],
                    'local_grav_crns': row['local_grav_crns']
                }
                stations.append(station)
            except Exception as e:
                print(e)
                print(f'Incompatible field formatting in row {reader.line_num}. Please correct this field and retry.')

    return stations


# ----- EXCEL SPREADSHEET READ METHODS -----


def get_available_site_numbers(site_descriptions: Dict[str, Any]) -> List[int]:
    site_numbers = [description['Site_Number'] for description in site_descriptions if description['Site_Number'] != None]
    site_numbers.sort()
    return site_numbers


def map_sheet_data_to_site_schema(description: Dict[str, Any], network_summary: Dict[str, Any], calibration_details: Dict[str, Any]) -> Dict[str, Any]:
    return {
        'site_no': int(description['Site_Number']),
        'site_name': description['Site_Name'],
        'tube_type': description['CRNS_Model'],
        'network': "CosmOz",
        'imei': str("" if description['IMEI'] == "NULL" or description['IMEI'] is None else description['IMEI']),
        'sat_data_select': description['SatDataSelect'],
        'hydroinnova_serial_no': None,
        'latitude': network_summary['Latitude'],
        'longitude': network_summary['Longitude'],
        'elevation': network_summary['Elevation'],  # Elevation is the more correct term to be used here as requested. However the column name in input file is unchanged at this time.
        'installation_date': description['Installation_Date'],
        'contact': description['Site_Contact'],
        'email': description['Email_Address'],
        'site_description': "" if description['Site_Description'] is None else description['Site_Description'],
        'calibration_type': "V",
        'timezone': str(network_summary['UTC_Offset_h']),
        'ref_pressure': network_summary['Reference_Pressure'],
        'ref_intensity': network_summary['Reference_Intensity'],
        'cutoff_rigidity': network_summary['Cutoff_Rigidity'],
        'elev_scaling': network_summary['Elevation_Scale'],
        'latit_scaling': network_summary['Latitude_Scale'],
        'scaling': 1,
        'beta': network_summary['Atmos_Atten_Coeff'],
        'n0_cal': network_summary['N0'],
        'bulk_density': network_summary['Bulk_Density'],
        'lattice_water_g_g': network_summary['Lattice'],
        'soil_organic_matter_g_g': network_summary['Organic_C'],
        'site_photo_name': None,
        'nmdb': network_summary['Dose_Site'],
        'tau': network_summary['Tau'],
        'local_grav_crns': network_summary['LocalGravCRNS']
    }


def get_station_information_from_excel(file_path: str, target_site_numbers: List[int] = None) -> Dict[str, Dict[str, Any]]:
    # Load raw workbook data into a dictionary of sheets and their contents
    workbook = openpyxl.load_workbook(file_path)
    sheets = workbook.sheetnames
    data = {}

    for sheet_name in sheets:
        sheet = workbook[sheet_name]
        rows = sheet.values
        headings = next(rows)
        sheet_data = []

        for row in rows:
            sheet_data.append(dict(zip(headings, row)))

        data[sheet_name] = sheet_data

    # Pull out the sheets relevant to the Cosmoz Station definitions
    descriptions = data['Descriptions']
    calibration_details = data['Calibration_Details']
    network_summaries = data['Network_Summary']

    # Get the available site numbers ignoring records where one is not specified
    available_site_numbers = get_available_site_numbers(descriptions)

    # Ensure that all target sites are valid, if specific targets have been supplied. Otherwise assume all available sites are being read.
    if target_site_numbers is None:
        target_site_numbers = available_site_numbers
    else:
        if not all(site_no in available_site_numbers for site_no in target_site_numbers):
            print(f"Target site numbers: {target_site_numbers}")
            print(f"Available site numbers: {available_site_numbers}")
            raise ValueError(f'One or more of the specified site numbers to be retrieved is not present in the summary spreadsheet.')
        
    # Translate sheet data into the CosmOz site/station database model.
    stations = []
    for description in descriptions:
        if description['Site_Number'] in target_site_numbers:
            key = description["Site_Name_Number"]

            ns = next((x for x in network_summaries if x['Site_Name'] == key), None)
            if ns is None:
                raise ValueError(f"No Network_Summary row found for site: '{key}'")
            
            cd = next((x for x in calibration_details if x['Site_Name'] == key), None)
            if cd is None:
                raise ValueError(f"No Calibration_Details row found for site: '{key}'")
            
            stations.append(map_sheet_data_to_site_schema(description, ns, cd))

    return stations
