#!/bin/bash
# Processes raw to level1, level1 to level2, level2 to level3, and level3 to level4
source ./.venv/bin/activate
PYTHONUNBUFFERED=1
export PYTHONUNBUFFERED

#Uncomment these to process to soils-discovery rather than in the container.
#export INFLUX_DB_HOST=soils-discovery.it.csiro.au
#export INFLUX_DB_PORT=8186
#export MONGO_DB_HOST=soils-discovery.it.csiro.au
#export MONGO_DB_PORT=27018

exec python3 ./restore_influxdb_raws.py
