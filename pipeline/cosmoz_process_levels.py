#!/bin/python3
# -*- coding: utf-8 -*-
"""
Copyright 2019 CSIRO Land and Water

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""
import argparse
import decimal
import math
import sys

from copy import copy
from datetime import datetime
from datetime import time as d_time
from datetime import timedelta, timezone
from multiprocessing import Pool
from multiprocessing.pool import ThreadPool
from typing import TYPE_CHECKING
from weakref import proxy

import reactivex as rx

from ciso8601 import parse_datetime
from influxdb_client import InfluxDBClient as InfluxDB2Client
from influxdb_client import Point, WritePrecision
from influxdb_client.client.write_api import WriteOptions
from pymongo import MongoClient
from reactivex import operators as ops
from sortedcontainers import SortedList
from urllib3 import Retry

from ._config import USE_COSMOZ_PUBLIC_API, NMDB_OPERATING_MODE
from ._influx2_db_config import config as influx2_config
from ._mongo_db_config import config as mongodb_config

from .utils import datetime_to_isostring
from .cosmoz_api import get_all_cosmoz_stations_from_public_api,  get_cosmoz_station_from_public_api

if TYPE_CHECKING:
    from influxdb_client.client.flux_table import FluxRecord

retries = Retry(connect=5, read=10, redirect=10)
scheme = "https" if int(influx2_config['DB_PORT']) in (443, 8443, 9443, 10443) else "http"

THIRTY_YEARS = timedelta(days=math.floor(365.25 * 30.0))
TEN_YEARS = timedelta(days=math.floor(365.25 * 10.0))
ONE_YEAR = timedelta(days=365)

# Multiprocessing is at the batch level, ie, it run the whole workflow for multiple sites at once
USE_MULTIPROCESS = False

# Multithreading is for performance tuning, it allows influxdb client to process more than one record at once.
# Note, this can cause data concurrency issues when looking for duplicates in level1,
# and when doing moving window smoothing in level4, but those should be sorted out by now
USE_MULTITHREAD = True

DEBUG_OUTPUT = False


def _make_l4_point(site_no, p, read_api, bulk_l3, bulk_l3_times):
    this_datetime = p.get_time()
    if DEBUG_OUTPUT:
        print("3 to 4 for Site: {} at Time: {}".format(site_no, this_datetime.isoformat()), flush=True)
    three_h_ago = this_datetime - timedelta(hours=3, seconds=120)
    three_h_fwd = this_datetime + timedelta(hours=3, seconds=120)

    read_from_db = True
    if bulk_l3 and bulk_l3_times:
        count = 0
        total_soil_moist = 0.0
        total_effective_depth = 0.0
        for i in bulk_l3_times.irange(three_h_ago, three_h_fwd, inclusive=(True, False)):
            count += 1
            record = bulk_l3[i]
            total_soil_moist += record['soil_moist']
            total_effective_depth += record['effective_depth']
        soil_moist_filtered = total_soil_moist / count
        depth_filtered = total_effective_depth / count
        read_from_db = False
    if read_from_db:
        avg_res = read_api.query(
            """\
          data = from(bucket: "cosmoz")
            |> range(start: start, stop: stop)
            |> filter(fn: (r) => r._measurement == "level3")
            |> filter(fn: (r) => r.site_no == s)
            |> filter(fn: (r) => r.flag == "0")
            |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
            |> keep(columns: ["_start", "soil_moist", "effective_depth"])
          avg1 = data |> mean(column: "soil_moist")
          avg2 = data |> mean(column: "effective_depth")
          join(tables: { effective_depth: avg1, soil_moist: avg2}, on: ["_start"], method: "inner")
          """,
            params={"start": three_h_ago, "stop": three_h_fwd, "s": str(site_no)},
        )

        avg_result_table = next(iter(avg_res))
        try:
            avgs = next(iter(avg_result_table))
            soil_moist_filtered = avgs['soil_moist']
            depth_filtered = avgs['effective_depth']
        except (StopIteration, KeyError):
            soil_moist_filtered = p['soil_moist']
            depth_filtered = p['effective_depth']

    return (
        Point("level4")
        .tag("site_no", p['site_no'])
        .time(p.get_time(), write_precision=WritePrecision.S)
        .field("soil_moist", float(p['soil_moist']))
        .field("effective_depth", float(p['effective_depth']))
        .field("rainfall", float(p['rainfall']))
        .field("soil_moist_filtered", float(soil_moist_filtered))
        .field("depth_filtered", float(depth_filtered))
    )


def level3_to_level4(ctx: dict, site_no=1, start_time=None, backprocess=None):
    if start_time is None:
        start_time = datetime.now().astimezone(timezone.utc)
    if backprocess is None:
        backprocess = TEN_YEARS
    back_time = start_time - backprocess
    total_delta = start_time - back_time
    total_days = ((total_delta.days * 86400) + total_delta.seconds) / 86400
    bulk_work = total_days > 7
    dryrun = ctx.get('dryrun', False)
    influx2_client = ctx['influx2_client']
    read_api = ctx['influx2_query_api']
    l3_res = read_api.query(
        """\
          from(bucket: "cosmoz")
            |> range(start: start)
            |> filter(fn: (r) => r._measurement == "level3")
            |> filter(fn: (r) => r.site_no == s)
            |> filter(fn: (r) => r.flag == "0")
            |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
            |> keep(columns: ["_time", "site_no", "soil_moist", "effective_depth", "rainfall"])
          """,
        params={"start": back_time, "s": str(site_no)},
    )
    l3_result_tables = iter(l3_res)
    try:
        l3_result_table = next(l3_result_tables)
    except StopIteration:
        print("No level3s to process for site: {} for this time period".format(site_no), flush=True)
        return
    if bulk_work:
        back_time_and_three = back_time.replace(minute=0, second=0) - timedelta(hours=3, seconds=120)
        bulk_read = read_api.query(
            """\
                 from(bucket: "cosmoz")
                   |> range(start: start)
                   |> filter(fn: (r) => r._measurement == "level3")
                   |> filter(fn: (r) => r.site_no == s)
                   |> filter(fn: (r) => r.flag == "0")
                   |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
                   |> keep(columns: ["_time", "soil_moist", "effective_depth"])
                 """,
            params={"start": back_time_and_three, "s": str(site_no)},
        )
        bulk_result_tables = iter(bulk_read)
        try:
            bulk_result_table = next(bulk_result_tables)
        except StopIteration:
            print("Level 4 bulk data read failed. Falling back.")
            bulk_read = False
            bulk_result_table = None
        bulk_l3 = {}
        while bulk_result_table is not None:
            for p in bulk_result_table:  # type: FluxRecord
                bulk_l3[p.get_time()] = p
            try:
                bulk_result_table = next(bulk_result_tables)
            except StopIteration:
                bulk_result_table = None
        bulk_l3_times = SortedList(bulk_l3.keys())
    else:
        bulk_l3 = None
        bulk_l3_times = None
    write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=1_000, flush_interval=5_000))
    if USE_MULTITHREAD:
        tp = ThreadPool(16)
        try:
            while l3_result_table is not None:
                data_observable = rx.from_(l3_result_table)
                tasks_array = []
                tasks_pipe = data_observable.pipe(
                    ops.map(
                        lambda p: tp.apply_async(_make_l4_point, (site_no, p, proxy(read_api), bulk_l3, bulk_l3_times))
                    )
                )
                _ = tasks_pipe.subscribe(
                    on_next=lambda t: tasks_array.append(t),
                    on_error=lambda ex: print(f'Unexpected error: {ex}', flush=True),
                )
                data_pipe = rx.from_(tasks_array).pipe(ops.map(lambda t: t.get()), ops.filter(lambda r: r is not None))
                if not dryrun:
                    write_api.write("cosmoz", write_precision=WritePrecision.S, record=data_pipe)
                try:
                    l3_result_table = next(l3_result_tables)
                except StopIteration:
                    l3_result_table = None
        finally:
            tp.close()
            write_api.close()
            del read_api
    else:
        try:
            while l3_result_table is not None:
                data_observable = rx.from_(l3_result_table)
                data_pipe = data_observable.pipe(
                    ops.map(lambda p: _make_l4_point(site_no, p, read_api, bulk_l3, bulk_l3_times)),
                    ops.filter(lambda r: r is not None),
                )
                if not dryrun:
                    write_api.write("cosmoz", write_precision=WritePrecision.S, record=data_pipe)
                try:
                    l3_result_table = next(l3_result_tables)
                except StopIteration:
                    l3_result_table = None
        finally:
            write_api.close()
            del read_api


def level2_to_level3(ctx: dict, site_no=1, start_time=None, backprocess=None):
    if start_time is None:
        start_time = datetime.now().astimezone(timezone.utc)
    if backprocess is None:
        backprocess = TEN_YEARS
    back_time = start_time - backprocess
    total_delta = start_time - back_time
    total_days = ((total_delta.days * 86400) + total_delta.seconds) / 86400
    bulk_work = total_days > 7
    dryrun = ctx.get('dryrun', False)
    mongo_client = ctx.get('mongo_client', None)
    if mongo_client is None:
        this_site = get_cosmoz_station_from_public_api(site_no)
    else:
        mdb = getattr(mongo_client, mongodb_config['DB_NAME'])
        all_stations_collection = mdb.all_stations
        this_site = all_stations_collection.find_one({'site_no': site_no})
    try:
        alternate_algorithm = this_site["alternate_algorithm"]
    except LookupError:
        alternate_algorithm = None
    sandy_a = 1216036430.0
    sandy_b = -3.272
    n0_cal = this_site['n0_cal']
    if not isinstance(n0_cal, float):
        try:
            n0_cal = float(n0_cal.to_decimal())
        except AttributeError:
            n0_cal = float(n0_cal)
    bulk_density = this_site['bulk_density']
    if not isinstance(bulk_density, float):
        try:
            bulk_density = float(bulk_density.to_decimal())
        except AttributeError:
            bulk_density = float(bulk_density)
    lattice_water_g_g = this_site['lattice_water_g_g']
    if not isinstance(lattice_water_g_g, (float, decimal.Decimal)):
        try:
            lattice_water_g_g = lattice_water_g_g.to_decimal()
        except AttributeError:
            lattice_water_g_g = float(lattice_water_g_g)
    soil_organic_matter_g_g = this_site['soil_organic_matter_g_g']
    if not isinstance(soil_organic_matter_g_g, (float, decimal.Decimal)):
        try:
            soil_organic_matter_g_g = soil_organic_matter_g_g.to_decimal()
        except AttributeError:
            soil_organic_matter_g_g = float(soil_organic_matter_g_g)
    lattice_soil_organic_sum = float(lattice_water_g_g + soil_organic_matter_g_g)

    influx2_client = ctx['influx2_client']
    read_api = ctx['influx2_query_api']

    l2_res = read_api.query(
        """\
       from(bucket: "cosmoz")
         |> range(start: start)
         |> filter(fn: (r) => r._measurement == "level2")
         |> filter(fn: (r) => r.site_no == s)
         |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
         |> keep(columns: ["_time", "site_no", "wv_corr", "corr_count", "rain", "rain_mm", "flag"])
         |> rename(columns: {flag: "level2_flag"})
       """,
        params={"start": back_time, "s": str(site_no)},
    )
    l2_result_tables = iter(l2_res)
    try:
        l2_result_table = next(l2_result_tables)
    except StopIteration:
        print("No level2s to process for site: {} for this time period".format(site_no), flush=True)
        del read_api  # close QueryAPI
        return
    write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=1_000, flush_interval=5_000))
    try:
        while l2_result_table is not None:
            for p in l2_result_table:
                this_datetime = p.get_time()
                if DEBUG_OUTPUT:
                    print("2 to 3 for Site: {} at Time: {}".format(site_no, this_datetime.isoformat()), flush=True)
                wv_corr = float(p['wv_corr'])
                corr_count = float(p['corr_count'])
                if alternate_algorithm and alternate_algorithm == "sandy":
                    if wv_corr == 1.0:
                        flag = 5
                    elif corr_count > (3.0 * n0_cal):
                        flag = 3
                    elif corr_count < (0.5 * n0_cal):
                        flag = 2
                    else:
                        flag = int(p['level2_flag'])
                    if corr_count < 1.0:
                        corrected_moist_val = 0.0
                    else:
                        corrected_moist_val = sandy_a * (corr_count**sandy_b)
                else:
                    if wv_corr == 1.0:
                        flag = 5
                    elif corr_count > n0_cal:
                        flag = 3
                    elif corr_count < (0.4 * n0_cal):
                        flag = 2
                    else:
                        flag = int(p['level2_flag'])
                    corrected_moist_val = (
                        0.0808 / ((corr_count / n0_cal) - 0.372) - 0.115 - lattice_soil_organic_sum
                    ) * bulk_density
                # ((0.0808 / ((l2.CorrCount / a.N0_Cal) - 0.372) - 0.115 - a.LatticeWater_g_g - a.SoilOrganicMatter_g_g) * a.BulkDensity) * 100
                soil_moisture = corrected_moist_val * 100.0
                # 5.8 / ( ((a.LatticeWater_g_g + a.SoilOrganicMatter_g_g) * a.BulkDensity) + ( (0.0808 / ( (l2.CorrCount / a.N0_Cal) - 0.372) - 0.115 - a.LatticeWater_g_g - a.SoilOrganicMatter_g_g) * a.BulkDensity ) + 0.0829) AS EffectiveDepth,
                effective_depth = 5.8 / ((lattice_soil_organic_sum * bulk_density) + corrected_moist_val + 0.0829)
                if "rain_mm" in p.values:
                    # rain is in mm
                    rainfall_val = float(p["rain_mm"])
                else:
                    rainfall_val = float(p['rain']) * 0.2
                if not dryrun:
                    write_api.write(
                        "cosmoz",
                        write_precision=WritePrecision.S,
                        record=Point("level3")
                        .tag("site_no", p['site_no'])
                        .tag("flag", int(flag))
                        .time(this_datetime, write_precision=WritePrecision.S)
                        .field("soil_moist", soil_moisture)
                        .field("effective_depth", effective_depth)
                        .field("rainfall", rainfall_val),
                    )
            try:
                l2_result_table = next(l2_result_tables)
            except StopIteration:
                l2_result_table = None
    finally:
        del read_api
        write_api.close()


def _make_l2_point(site_no, site_data, p, read_api, nmdb_times, nmdb_records):
    this_datetime = p.get_time()
    if DEBUG_OUTPUT:
        print("1 to 2 for Site: {} at Time: {}".format(site_no, this_datetime.isoformat()), flush=True)
    count = p['count']
    pressure1 = float(p['pressure1'])
    pressure2 = float(p['pressure2'])
    beta = site_data['beta']
    ref_pressure = site_data['ref_pressure']
    if not isinstance(beta, float):
        try:
            beta = float(beta.to_decimal())
        except AttributeError:
            beta = float(beta)
    if not isinstance(ref_pressure, float):
        try:
            ref_pressure = float(ref_pressure.to_decimal())
        except AttributeError:
            ref_pressure = float(ref_pressure)

    # Check pressure readings values fall within reasonable upper and lower limits
    if pressure2 > 700 and pressure2 < 1400:
        press_corr = math.exp(beta * (pressure2 - ref_pressure))
    elif pressure1 > 700 and pressure1 < 1400:
        press_corr = math.exp(beta * (pressure1 - ref_pressure))
    else:
        press_corr = 1.0
    
    this_day_start = datetime.combine(this_datetime.date(), d_time(0, 0, 0, 0, tzinfo=this_datetime.tzinfo))
    this_day_end = datetime.combine(this_datetime.date(), d_time(23, 59, 59, 999999, tzinfo=this_datetime.tzinfo))
    this_hour_start = datetime.combine(
        this_datetime.date(), d_time(this_datetime.hour, 0, 0, 0, tzinfo=this_datetime.tzinfo)
    )
    this_hour_end = datetime.combine(
        this_datetime.date(), d_time(this_datetime.hour, 59, 59, 999999, tzinfo=this_datetime.tzinfo)
    )
    external_temperature = float(p['external_temperature'])
    external_humidity = float(p['external_humidity'])
    # if external temperature or external humidity is zero, we will need to get the data from SILO.
    if external_temperature == 0 or external_humidity == 0:
        silo_query = """\
                from(bucket: "cosmoz")
                  |> range(start: start, stop: stop)
                  |> filter(fn: (r) => r._measurement == "silo_data")
                  |> filter(fn: (r) => r.site_no == s)
                  |> last(column: "_time")
                  |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
                  |> keep(columns: ["average_humidity", "average_temperature", "_time", "site_no"])
                """
        resp = read_api.query(
            silo_query, params={"start": this_day_start, "stop": this_day_end, "s": str(p['site_no'])}
        )
        try:
            silo_table = next(iter(resp))
            sp = next(iter(silo_table))
            average_temperature = float(sp['average_temperature'])
            average_humidity = float(sp['average_humidity'])
        except (StopIteration, ValueError, KeyError):
            average_temperature = None
            average_humidity = None
    else:
        average_temperature = None
        average_humidity = None

    if (
        external_temperature is not None
        and external_humidity is not None
        and external_temperature != 0
        and external_humidity != 0
    ):
        # IF ExternalTemperature AND ExternalHumidity (from the Level1View View) has valid data. Use them in the WVCorr equation
        wv_corr_store = 1 + 0.0054 * (
            (
                2165
                * (
                    (0.6108 * math.exp((17.27 * external_temperature) / (external_temperature + 237.3)))
                    * (external_humidity / 100.0)
                )
            )
            / (external_temperature + 273.16)
            - 0
        )
        wv_corr_use = wv_corr_store
    elif average_humidity is not None:
        # Otherwise, IF AverageTemperature AND AverageHumidity (from the SiloData table) has valid data. Use them in the WVCorr equation.
        use_temp = average_temperature if average_temperature is not None else 0.0
        wv_corr_use = 1 + 0.0054 * (
            (2165 * ((0.6108 * math.exp((17.27 * use_temp) / (use_temp + 237.3))) * (average_humidity / 100.0)))
            / (use_temp + 273.16)
            - 0
        )
        wv_corr_store = wv_corr_use
    else:
        # Finally, use either external OR average values, or zero
        use_humidity = average_humidity if external_humidity == 0 else external_humidity
        use_temp = average_temperature if external_temperature == 0 else external_temperature
        if use_humidity is None or use_humidity == 0:
            wv_corr_use = 1.0
        else:
            if use_temp is None:
                use_temp = 0.0  # Use the actual zero temp in the calculation
            wv_corr_use = 1 + 0.0054 * (
                (2165 * ((0.6108 * math.exp((17.27 * use_temp) / (use_temp + 237.3))) * (use_humidity / 100.0)))
                / (use_temp + 273.16)
                - 0
            )
        wv_corr_store = wv_corr_use
    # IF we can match the record's timestamp (to the hour) to one in the Intensity table. Use the Intensity value in the IntensityCorr equation.
    try:
        intensity_time = next(nmdb_times.irange(this_hour_start, this_hour_end, inclusive=(True, True)))
        intensity_p = nmdb_records[intensity_time]
    except StopIteration:
        # no intensity to the nearest hour
        # Otherwise, IF we can find the last valid timestamp for this record. Use the Intensity value in the IntensityCorr equation.
        try:
            intensity_time = next(
                nmdb_times.irange(
                    datetime.min.replace(tzinfo=timezone.utc), this_hour_end, inclusive=(True, False), reverse=True
                )
            )
            intensity_p = nmdb_records[intensity_time]
        except StopIteration:
            # no intensity found still
            # Search forward in time, get the next one it sees in the future?
            try:
                intensity_time = next(
                    nmdb_times.irange(
                        this_hour_start, datetime.max.replace(tzinfo=timezone.utc), inclusive=(False, True)
                    )
                )
                intensity_p = nmdb_records[intensity_time]
            except StopIteration:
                intensity_p = None
            except Exception as e:
                raise
        except Exception as e:
            raise
    except Exception as e:
        print(e)
        raise

    if intensity_p:
        try:
            use_intensity = float(intensity_p.values["_value"])
        except (KeyError, LookupError):
            use_intensity = 0.0
        if use_intensity == 0.0:  # prevent div by zero
            intensity_corr = 1.0
        else:
            ref_intensity = site_data['ref_intensity']
            if not isinstance(ref_intensity, float):
                try:
                    ref_intensity = float(ref_intensity.to_decimal())
                except AttributeError:
                    ref_intensity = float(ref_intensity)

            tau = site_data['tau']
            if not isinstance(tau, float):
                try:
                    tau = float(tau.to_decimal())
                except:
                    tau = float(tau)

            intensity_corr = (tau * (use_intensity / ref_intensity) + 1 - tau) ** -1
    else:
        intensity_corr = 1.0
    latit_scaling = site_data['latit_scaling']
    elev_scaling = site_data['elev_scaling']
    if not isinstance(latit_scaling, (float, decimal.Decimal)):
        try:
            latit_scaling = latit_scaling.to_decimal()
        except AttributeError:
            latit_scaling = float(latit_scaling)
    if not isinstance(elev_scaling, (float, decimal.Decimal)):
        try:
            elev_scaling = elev_scaling.to_decimal()
        except AttributeError:
            elev_scaling = float(elev_scaling)
    try:
        corr_count = (float(count) * press_corr * wv_corr_use * intensity_corr * (float(elev_scaling / latit_scaling)))
    except ZeroDivisionError:
        print("count:", p["count"])
        print("latit_scaling:", latit_scaling)
        print("elev_scaling:", elev_scaling)
        print("wv_corr_use:", wv_corr_use)
        print("intensity_corr:", intensity_corr)
        raise
    if "rain_mm" in p.values:
        # rain is in mm
        rain_field = "rain_mm"
    else:
        rain_field = "rain"
    return (
        Point("level2")
        .tag("site_no", p['site_no'])
        .tag("flag", int(p['level1_flag']))
        .time(this_datetime, write_precision=WritePrecision.S)
        .field("count", int(count))
        .field("press_corr", press_corr)
        .field("wv_corr", wv_corr_store)
        .field("intensity_corr", intensity_corr)
        .field("corr_count", corr_count)
        .field(rain_field, float(p[rain_field]))
    )


def get_intensities_for_site(ctx: dict, site_no, range_start: datetime, range_end: datetime):
    read_api = ctx['influx2_query_api']
    cache = ctx.get('intensities_cache', None)
    if cache is None:
        ctx['intensities_cache'] = cache = {}

    try:
        all_times, all_mapped = cache[(site_no, round(range_start.timestamp()), round(range_end.timestamp()))]
        return all_times, all_mapped
    except LookupError:
        pass
    # 2262 bug
    # https://en.wikipedia.org/wiki/Time_formatting_and_storage_bugs#Year_2262
    intensities_query = """\
    a = from(bucket: "cosmoz")
        |> range(start: 1970-01-01T00:00:00.000Z, stop: start)
        |> filter(fn: (r) => r._measurement == "intensity")
        |> filter(fn: (r) => r._field == "intensity")
        |> filter(fn: (r) => r.bad_data_flag == "0")
        |> filter(fn: (r) => r.site_no == s)
        |> last(column: "_time")
    b = from(bucket: "cosmoz")
        |> range(start: start, stop: stop)
        |> filter(fn: (r) => r._measurement == "intensity")
        |> filter(fn: (r) => r._field == "intensity")
        |> filter(fn: (r) => r.bad_data_flag == "0")
        |> filter(fn: (r) => r.site_no == s)
    c = from(bucket: "cosmoz")
        |> range(start: stop, stop: 2262-04-11T23:47:16.854Z)
        |> filter(fn: (r) => r._measurement == "intensity")
        |> filter(fn: (r) => r._field == "intensity")
        |> filter(fn: (r) => r.bad_data_flag == "0")
        |> filter(fn: (r) => r.site_no == s)
        |> first(column: "_time")
    union(tables: [a, b, c])
        |> yield()"""
    intensity_req = read_api.query(
        intensities_query, params={"start": range_start, "stop": range_end, "s": str(site_no)}
    )
    intensity_tables = iter(intensity_req)
    try:
        intensity_table = next(intensity_tables)
    except StopIteration:
        print("Error getting nmdb intensities for site: {}".format(site_no))
        return
    all_mapped = {}
    while intensity_table is not None:
        for p in intensity_table:  # type: FluxRecord
            all_mapped[p.get_time()] = p
        try:
            intensity_table = next(intensity_tables)
        except StopIteration:
            intensity_table = None
    all_times = SortedList(all_mapped.keys())

    if len(all_mapped) < 1:
        print("Error getting nmdb intensities for site: {}".format(site_no))
    # Save it, to prevent looking it up again next time
    cache[(site_no, round(range_start.timestamp()), round(range_end.timestamp()))] = (all_times, all_mapped)
    return all_times, all_mapped


def get_hourly_intensities(ctx: dict, range_start: datetime, range_end: datetime):
    # Site 0 is uses to always refer to hourly intensities from JUNG reference site.
    read_api = ctx['influx2_query_api']
    cache = ctx.get('intensities_cache', None)
    if cache is None:
        ctx['intensities_cache'] = cache = {}

    try:
        all_times, all_mapped = cache[(0, round(range_start.timestamp()), round(range_end.timestamp()))]
        return all_times, all_mapped
    except LookupError:
        pass
    # This gets one record before range_start, then all the range, then one record after range_start
    intensities_query = """\
    a = from(bucket: "cosmoz")
        |> range(start: 1970-01-01T00:00:00.000Z, stop: start)
        |> filter(fn: (r) => r._measurement == "intensity")
        |> filter(fn: (r) => r._field == "intensity")
        |> filter(fn: (r) => r.bad_data_flag == "0")
        |> filter(fn: (r) => r.site_no == "0")
        |> last(column: "_time")
    b = from(bucket: "cosmoz")
        |> range(start: start, stop: stop)
        |> filter(fn: (r) => r._measurement == "intensity")
        |> filter(fn: (r) => r._field == "intensity")
        |> filter(fn: (r) => r.bad_data_flag == "0")
        |> filter(fn: (r) => r.site_no == "0")
    c = from(bucket: "cosmoz")
        |> range(start: stop, stop: 2262-04-11T23:47:16.854Z)
        |> filter(fn: (r) => r._measurement == "intensity")
        |> filter(fn: (r) => r._field == "intensity")
        |> filter(fn: (r) => r.bad_data_flag == "0")
        |> filter(fn: (r) => r.site_no == "0")
        |> first(column: "_time")
    union(tables: [a, b, c])
        |> yield()"""
    # 2262 bug                ^^
    # https://en.wikipedia.org/wiki/Time_formatting_and_storage_bugs#Year_2262

    intensity_req = read_api.query(
        intensities_query, params={"start": range_start, "stop": range_end}
    )
    intensity_tables = iter(intensity_req)
    try:
        intensity_table = next(intensity_tables)
    except StopIteration:
        print("Error getting reference nmdb intensities from DB: {}")
        return
    all_mapped = {}
    while intensity_table is not None:
        for p in intensity_table:  # type: FluxRecord
            all_mapped[p.get_time()] = p
        try:
            intensity_table = next(intensity_tables)
        except StopIteration:
            intensity_table = None
    all_times = SortedList(all_mapped.keys())

    if len(all_mapped) < 1:
        print("Error getting reference nmdb intensities from DB: {}")
    # Save it, to prevent looking it up again next time
    cache[(0, round(range_start.timestamp()), round(range_end.timestamp()))] = (all_times, all_mapped)
    return all_times, all_mapped


def level1_to_level2(ctx: dict, site_no=1, start_time=None, backprocess: timedelta=None):
    if start_time is None:
        start_time: datetime = datetime.now().astimezone(timezone.utc)
    this_hour_end: datetime = start_time.replace(minute=59, second=59, microsecond=999999)
    if backprocess is None:
        backprocess = TEN_YEARS
    back_time: datetime = start_time - backprocess
    first_hour_start: datetime = back_time.replace(minute=0, second=0, microsecond=0)

    total_delta: timedelta = start_time - back_time
    total_days = ((total_delta.days * 86400) + total_delta.seconds) / 86400
    bulk_work = total_days > 7
    dryrun = ctx.get('dryrun', False)
    mongo_client = ctx.get('mongo_client', None)
    if mongo_client is None:
        this_site = get_cosmoz_station_from_public_api(site_no)
    else:
        mdb = getattr(mongo_client, mongodb_config['DB_NAME'])
        all_stations_collection = mdb.all_stations
        this_site = all_stations_collection.find_one({'site_no': site_no})
    influx2_client = ctx['influx2_client']
    read_api = ctx['influx2_query_api']

    l1_res = read_api.query(
        """\
    from(bucket: "cosmoz")
      |> range(start: start)
      |> filter(fn: (r) => r._measurement == "level1")
      |> filter(fn: (r) => r.site_no == s)
      |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
      |> keep(columns: ["_time", "site_no", "count", "pressure1", "pressure2", "external_temperature", "external_humidity", "rain", "rain_mm", "flag"])
      |> rename(columns: {flag: "level1_flag"})
    """,
        params={"start": back_time, "s": str(site_no)},
    )
    l1_result_tables = iter(l1_res)
    try:
        l1_result_table = next(l1_result_tables)
    except StopIteration:
        print("No level1s to process for site: {} for this time period".format(site_no), flush=True)
        del read_api  # close QueryApi
        return
    # pre-load all NMDB intensities for this site, for performance reasons
    print("Pre-loading nmdb intensities for site: {} ... ".format(site_no), end="", flush=True)
    if NMDB_OPERATING_MODE == "SIMPLIFIED":
        r = get_hourly_intensities(ctx, first_hour_start, this_hour_end)
    else:
        r = get_intensities_for_site(ctx, site_no, first_hour_start, this_hour_end)
    if r is None or len(r) < 1:
        print("Cannot find any nmdb intensities for site: {}, skipping it. ".format(site_no), end="", flush=True)
        del read_api  # close QueryApi
        return
    else:
        nmdb_times, nmdb_records = r
        print("Done.")
    write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=1_000, flush_interval=5_000))
    if USE_MULTITHREAD:
        tp = ThreadPool(16)
        try:
            while l1_result_table is not None:
                data_observable = rx.from_(l1_result_table)
                tasks_array = []
                tasks_pipe = data_observable.pipe(
                    ops.map(
                        lambda p: tp.apply_async(
                            _make_l2_point, (site_no, copy(this_site), p, proxy(read_api), nmdb_times, nmdb_records)
                        )
                    )
                )
                _ = tasks_pipe.subscribe(
                    on_next=lambda t: tasks_array.append(t), on_error=lambda ex: print(f'Unexpected error: {ex}')
                )
                data_pipe = rx.from_(tasks_array).pipe(ops.map(lambda t: t.get()), ops.filter(lambda r: r is not None))
                if not dryrun:
                    write_api.write("cosmoz", write_precision=WritePrecision.S, record=data_pipe)
                try:
                    l1_result_table = next(l1_result_tables)
                except StopIteration:
                    l1_result_table = None
        finally:
            tp.close()
            write_api.close()
            del read_api
    else:
        try:
            while l1_result_table is not None:
                data_observable = rx.from_(l1_result_table)
                data_pipe = data_observable.pipe(
                    ops.map(lambda p: _make_l2_point(site_no, this_site, p, read_api, nmdb_times, nmdb_records)),
                    ops.filter(lambda r: r is not None),
                )
                if not dryrun:
                    write_api.write("cosmoz", write_precision=WritePrecision.S, record=data_pipe)
                try:
                    l1_result_table = next(l1_result_tables)
                except StopIteration:
                    l1_result_table = None
        finally:
            write_api.close()
            del read_api


def is_duplicate(ctx: dict, site_no, record1, record2, table):
    one_second = timedelta(days=0, seconds=1)
    if isinstance(record2, str):
        record2 = parse_datetime(record2)
    if isinstance(record2, datetime):
        read_api = ctx['influx2_query_api']

        d_res = read_api.query(
            """\
        from(bucket: "cosmoz")
          |> range(start: start_time, stop: stop_time)
          |> filter(fn: (r) => r._measurement == table and r.site_no == s)
          |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
          |> keep(columns: ["_time", "site_no", "count", "pressure1", "internal_temperature", "internal_humidity", "battery", "tube_temperature", "tube_humidity", "rain", "rain_mm", "vwc1", "vwc2", "vwc3", "pressure2", "external_temperature", "external_humidity", "flag"])
          |> rename(columns: {flag: "raw_flag"})
        """,
            params={"table": table, "start_time": record2, "stop_time": record2 + one_second, "s": str(site_no)},
        )
        try:
            d_res_table = next(iter(d_res))
            record2 = next(iter(d_res_table))
        except (IndexError, StopIteration):
            return False
        finally:
            del read_api  # close QueryApi
    different = {}
    time = record2.get_time()
    for key, val in record1.values.items():
        if key in ("_time", "site_no", "flag", "raw_flag", "count_diff", "table", "result", "_start", "_stop"):
            continue
        do_float = key not in ("count",)
        if do_float:
            val = round(float(val), 2)
        try:
            val2 = record2[key]
            if do_float:
                val2 = round(float(val2), 2)
            if val != val2:
                different[key] = (val, val2)
        except (KeyError, LookupError, ValueError):
            pass
    if len(different) < 1:
        return time
    return False


def raw_to_level1(ctx: dict, site_no=1, start_time=None, backprocess=None):
    if start_time is None:
        start_time = datetime.now().astimezone(timezone.utc)
    if backprocess is None:
        backprocess = TEN_YEARS
    dryrun = ctx.get('dryrun', False)
    back_time = start_time - backprocess
    total_delta = start_time - back_time
    total_days = ((total_delta.days * 86400) + total_delta.seconds) / 86400
    bulk_work = total_days > 7
    time_string = datetime_to_isostring(back_time)
    influx2_client = ctx['influx2_client']
    read_api = ctx['influx2_query_api']

    # First, get the time point of all existing records.
    # this is to help find duplicate entries when processing new raw entries
    # note, 'count' queried here too, because at least one data field
    # needs to exist in the query.
    a_res = read_api.query(
        """\
from(bucket: "cosmoz")
  |> range(start: 1970-01-01T00:00:00.000Z, stop: now())
  |> filter(fn: (r) => r._measurement == "raw_values" and r.site_no == s and exists r.flag)
  |> filter(fn: (r) => r._field == "count")
  |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
  |> keep(columns: ["_time", "count", "site_no"])
""",
        params={"s": str(site_no)},
    )
    try:
        a_result_table = next(iter(a_res))
    except StopIteration:
        print("No raws to process for site: {} for this time period".format(site_no), flush=True)
        del read_api  # close read_api
        return
    all_mapped = {}
    for p in a_result_table:  # type: FluxRecord
        all_mapped[p.get_time()] = p
    all_times = SortedList(all_mapped.keys())
    extra_mapped = {}
    # Now query all values for records in our given processing time range
    # This also includes a calculated column called count_diff, that is
    # the difference between last 'count' and this 'count', row-wise.
    raw_res = read_api.query(
        """\
from(bucket: "cosmoz")
  |> range(start: start)
  |> filter(fn: (r) => r._measurement == "raw_values" and r.site_no == s and exists r.flag)
  |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
  |> keep(columns: ["_time", "site_no", "count", "pressure1", "internal_temperature", "internal_humidity", "battery", "tube_temperature", "tube_humidity", "rain", "rain_mm", "vwc1", "vwc2", "vwc3", "pressure2", "external_temperature", "external_humidity", "flag"])
  |> rename(columns: {flag: "raw_flag"})
  |> duplicate(column: "count", as: "count_diff")
  |> difference(columns: ["count_diff"], keepFirst: true)
""",
        params={"start": back_time, "s": str(site_no)},
    )
    raws_result_tables = iter(raw_res)
    try:
        raws_result_table = next(raws_result_tables)
    except StopIteration:
        print("No raws to process for site: {} for this time period".format(site_no), flush=True)
        del read_api  # close read_api
        return
    write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=1_000, flush_interval=5_000))
    try:
        # Note! Important! Don't run this one multithreaded.
        # The loop needs to be able to flush the batch writer to
        # check for duplicates in the db. You can't do that in threads.
        while raws_result_table is not None:
            for p in raws_result_table:
                at_time = p.get_time()
                if DEBUG_OUTPUT:
                    print("R to 1 for Site: {} at Time: {}".format(site_no, at_time.isoformat()), flush=True)
                count = p['count']
                dup_back_time = at_time - timedelta(minutes=29.0)
                possible_duplicates_times = list(all_times.irange(dup_back_time, at_time, inclusive=(True, False)))
                if len(possible_duplicates_times) > 0:
                    for dt in possible_duplicates_times:
                        try:
                            # Check if the duplicate is a time previously read from the database.
                            dc = all_mapped[dt]['count']
                            dt_time = all_mapped[dt].get_time()
                            force_flush = False
                        except LookupError:
                            # This duplicate is one we just added (it's not in the db yet!)
                            dc = extra_mapped[dt]['count']
                            dt_time = extra_mapped[dt].get_time()
                            force_flush = True
                        if dc == count:
                            if force_flush:
                                print(
                                    "Candidate for duplicate record found. Flushing working records to DB before checking..."
                                )
                                write_api.close()  # closing write_api is the only way to flush a batch writer
                                # recreate the write_api with same params as before
                                write_api = influx2_client.write_api(
                                    write_options=WriteOptions(batch_size=1_000, flush_interval=5_000)
                                )
                                all_mapped.update(extra_mapped)
                                extra_mapped.clear()
                            else:
                                print("Candidate for duplicate record found, looking up db to double check.")
                            is_duplicate_time = is_duplicate(ctx, site_no, p, dt_time, 'raw_values')
                            if is_duplicate_time:
                                break
                    else:
                        is_duplicate_time = False
                    if is_duplicate_time:
                        print(
                            "Skipping time {} at site {} because it is a duplicate with time {}".format(
                                datetime_to_isostring(at_time), site_no, datetime_to_isostring(is_duplicate_time)
                            )
                        )
                        continue
                if at_time in all_times:
                    if at_time in all_mapped:
                        del all_mapped[at_time]
                else:
                    all_times.add(at_time)
                extra_mapped[at_time] = p
                count_diff = p['count_diff']
                if count_diff is None:
                    count_diff = 0
                prev_count = count + (count_diff * -1.0)
                if count < 0 or count > 100_000:
                    bad_count = True
                    count = 0
                else:
                    if prev_count < 0:
                        bad_count = True
                        prev_count = 0
                    else:
                        bad_count = False
                bat = float(p['battery'])
                if 0.0 < bat < 10.0:
                    flag = 4
                elif bad_count or count < (0.8 * prev_count) or count > (1.2 * prev_count):
                    flag = 1
                else:
                    flag = p['raw_flag']
                if "rain_mm" in p.values:
                    # rain is in mm
                    rain_field = "rain_mm"
                else:
                    rain_field = "rain"
                if p['pressure1'] is None:
                    print("here!")
                if not dryrun:
                    # on level1, flag is int
                    write_api.write(
                        "cosmoz",
                        write_precision=WritePrecision.S,
                        record=Point("level1")
                        .tag("site_no", p['site_no'])
                        .tag("flag", int(flag))
                        .time(at_time, write_precision=WritePrecision.S)
                        .field("count", int(p['count']))
                        .field("pressure1", float(p['pressure1']))
                        .field("internal_temperature", float(p['internal_temperature']))
                        .field("internal_humidity", float(p['internal_humidity']))
                        .field("battery", bat)
                        .field("tube_temperature", float(p['tube_temperature']))
                        .field("tube_humidity", float(p['tube_humidity']))
                        .field(rain_field, float(p[rain_field]))
                        .field("vwc1", float(p['vwc1']))
                        .field("vwc2", float(p['vwc2']))
                        .field("vwc3", float(p['vwc3']))
                        .field("pressure2", float(p['pressure2']))
                        .field("external_temperature", float(p['external_temperature']))
                        .field("external_humidity", float(p['external_humidity'])),
                    )
            try:
                raws_result_table = next(raws_result_tables)
            except StopIteration:
                raws_result_table = None
    finally:
        del read_api
        write_api.close()


def make_context(options: dict):
    """A bunch of settings and objects that might need to be used by multiple steps, or passed between steps"""
    ctx = {}
    use_cosmoz_public_api = options.get('use_cosmoz_public_api', False)
    dryrun = options.get('dryrun', False)
    drop_old = options.get('drop_old', False)
    do_tests = options.get('do_tests', False)
    if drop_old:
        print(RuntimeWarning("option drop_old is no longer supported on new processing pipeline."))
    if do_tests:
        print(RuntimeWarning("option do_tests is no longer relevant or useful on new processing pipeline."))
    if use_cosmoz_public_api:
        ctx['mongo_client'] = None
    else:
        ctx['mongo_client'] = MongoClient(mongodb_config['DB_HOST'], int(mongodb_config['DB_PORT']))  # 27017

    influx2_client = InfluxDB2Client(
        "{}://{}:{}".format(scheme, influx2_config['DB_HOST'], int(influx2_config['DB_PORT'])),
        influx2_config['DB_TOKEN'],
        org=influx2_config["DB_ORGANIZATION"],
        timeout=30000,
        retries=retries,
    )
    ctx['influx2_client'] = influx2_client
    ctx['influx2_query_api'] = influx2_client.query_api()
    ctx['dryrun'] = dryrun

    return ctx

def process_levels(ctx: dict, site_no, options={}):
    start_time = options.get('start_time', None)
    backprocess = options.get('backprocess', None)
    p_start_time = datetime.now().astimezone(timezone.utc)
    if start_time is None:
        start_time = p_start_time
    print("Starting process_levels for site {}, at {}".format(site_no, p_start_time))
    raw_to_level1(ctx=ctx, site_no=site_no, start_time=start_time, backprocess=backprocess)
    print("Finished raw->level1 for site {}, starting level1->level2.".format(site_no))
    level1_to_level2(ctx=ctx, site_no=site_no, start_time=start_time, backprocess=backprocess)
    print("Finished level1->level2 for site {}, starting level2->level3.".format(site_no))
    level2_to_level3(ctx=ctx, site_no=site_no, start_time=start_time, backprocess=backprocess)
    print("Finished level2->level3 for site {}, starting level3->level4.".format(site_no))
    level3_to_level4(ctx=ctx, site_no=site_no, start_time=start_time, backprocess=backprocess)
    p_end_time = datetime.now().astimezone(timezone.utc)
    print("Finished process_levels for site {}, at {}".format(site_no, p_end_time))
    print("Site {} process_levels took {}".format(site_no, (p_end_time - p_start_time)))


def main():
    start_time = datetime.now().astimezone(timezone.utc)
    parser = argparse.ArgumentParser(description='Run the processing levels on the cosmoz influxdb.')

    parser.add_argument('-s', '--site-number', type=str, dest="siteno", help='Pick just one site number')
    parser.add_argument(
        '-d',
        '--process-days',
        type=str,
        dest="processdays",
        help='Number of days to backprocess. Default is 365 days.',
    )
    parser.add_argument(
        '-xx',
        '--dropold',
        dest="drop_old",
        action="store_true",
        help='Drop old contents of table before processing its contents. USE WITH CAUTION!',
    )
    parser.add_argument(
        '-t',
        '--from-datetime',
        type=str,
        dest="fromdatetime",
        help='The earliest datetime to backprocess to. In isoformat. Default is all of history.\nNote cannot use -d and -t together.',
    )
    parser.add_argument(
        '-o',
        '--output',
        dest='output',
        nargs='?',
        type=argparse.FileType('w'),
        help='Send output to a file (defaults to stdout).',
        default=sys.stdout,
    )
    parser.add_argument(
        '--dry-run',
        dest='dryrun',
        action='store_true',
        help='Run without updating the DB',
        default=False,
    )
    args = parser.parse_args()
    outfile = args.output

    def printout(*values, sep=' ', end='\n'):
        return print(*values, sep=sep, end=end, file=outfile, flush=True)

    try:
        processdays = args.processdays
        fromdatetime = args.fromdatetime
        drop_old = args.drop_old
        siteno = args.siteno
        dryrun = args.dryrun
        if processdays is not None and fromdatetime is not None:
            raise RuntimeError("Cannot use -d and -t at the same time. Pick one.")
        if processdays:
            try:
                processdays = int(processdays)
            except:
                raise RuntimeError("-d must be an integer")
            backprocess = timedelta(days=processdays)
        else:
            if fromdatetime is None:
                backprocess = ONE_YEAR
            else:
                fromdatetime = parse_datetime(fromdatetime)
                backprocess = start_time - fromdatetime
        if backprocess.days < 0:
            raise RuntimeError("Cannot backprocess negative time. Ensure it is positive.")
        worker_options = {
            'start_time': start_time,
            'do_tests': False,
            'backprocess': backprocess,
            'drop_old': drop_old,
            'dryrun': dryrun,
            'use_cosmoz_public_api': True if USE_COSMOZ_PUBLIC_API else False
        }
        ctx = make_context(worker_options)
        if not ctx['mongo_client']:
            all_stations_map = get_all_cosmoz_stations_from_public_api()
            all_stations = all_stations_map.values()
            if siteno is not None:
                filtered_stations = []
                sitenos = [s.strip() for s in siteno.split(',') if s]
                filtered_stations = [s for s in all_stations if str(s['site_no']) in sitenos]
                all_stations = filtered_stations
        else:
            mongo_client = ctx['mongo_client']
            mdb = getattr(mongo_client, mongodb_config['DB_NAME'])
            all_stations_docs = mdb.all_stations
            if siteno is not None:
                sitenos = [int(s.strip()) for s in siteno.split(',') if s]
                all_stations = all_stations_docs.find({'site_no': {"$in": sitenos}}, {'site_no': 1})
            else:
                all_stations = all_stations_docs.find({}, {'site_no': 1})
            mongo_client.close()
            all_stations = list(all_stations)  # This turns the mongo cursor into a python list

        if len(all_stations) < 1:
            printout("No stations to process.")
            return
        elif len(all_stations) < 2:
            printout("Only doing station {}".format(siteno))
            process_levels(ctx, all_stations[0]['site_no'], worker_options)
            end_time = datetime.now().astimezone(timezone.utc)
            printout("Finished process_levels for site {} at {}".format(siteno, end_time))
            printout("process_levels took {}".format((end_time - start_time)))
        elif USE_MULTIPROCESS is False:
            printout("Not using multiprocessing")
            for s in all_stations:
                process_levels(ctx, s['site_no'], worker_options)
            end_time = datetime.now().astimezone(timezone.utc)
            printout("Finished process_levels for All Sites at {}".format(end_time))
            printout("All sites process_levels took {}".format((end_time - start_time)))
        else:
            printout("Using multiprocessing")
            processes = []
            pool = Pool(6)  # uses os.cpu_count
            with pool as p:
                worker_args = [(ctx, s['site_no'], worker_options) for s in all_stations]
                p.starmap(process_levels, worker_args)
            end_time = datetime.now().astimezone(timezone.utc)
            printout("Finished process_levels for All Sites at {}".format(end_time))
            printout("All sites process_levels took {}".format((end_time - start_time)))
    finally:
        if outfile is not sys.stderr and outfile is not sys.stdout:
            outfile.close()


if __name__ == "__main__":
    main()
