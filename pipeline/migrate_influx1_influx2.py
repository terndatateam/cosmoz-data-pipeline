#!/bin/python3
# -*- coding: utf-8 -*-
"""
Copyright 2022 CSIRO Land and Water

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import math

import reactivex as rx

from ciso8601 import parse_datetime
from influxdb import InfluxDBClient as InfluxDB1Client
from influxdb_client import InfluxDBClient as InfluxDB2Client
from influxdb_client import Point, WritePrecision
from influxdb_client.client.write_api import WriteOptions
from pymongo import MongoClient
from reactivex import operators as ops
from urllib3 import Retry

from ._influx2_db_config import config as influx2_config
from ._influx_db_config import config as influx_config
from ._mongo_db_config import config as mongodb_config


influx1_client = InfluxDB1Client(
    influx_config['DB_HOST'],
    int(influx_config['DB_PORT']),
    influx_config['DB_USERNAME'],
    influx_config['DB_PASSWORD'],
    influx_config['DB_NAME'],
    timeout=30,
)

retries = Retry(connect=5, read=3, redirect=5)

scheme = "https" if int(influx2_config['DB_PORT']) in (443, 8443, 9443, 10443) else "http"
influx2_client = InfluxDB2Client(
    "{}://{}:{}".format(scheme, influx2_config['DB_HOST'], int(influx2_config['DB_PORT'])),
    influx2_config['DB_TOKEN'],
    org=influx2_config["DB_ORGANIZATION"],
    timeout=30000,
    retries=retries,
)


def do_power_data(site_no):
    result = influx1_client.query(
        """\
    SELECT "time", site_no, flag, * FROM "power_cycle_data"
    WHERE site_no=$s;""",
        bind_params={"s": str(site_no)},
    )

    def make_influx_point(p: dict):
        return (
            Point("power_cycle_data")
            .tag("site_no", p['site_no'])
            .tag("flag", int(p.get('flag', 0)))
            .time(parse_datetime(p['time']), write_precision=WritePrecision.S)
            .field("cycle", bool(p['cycle']))
        )

    data = rx.from_iterable(result.get_points()).pipe(ops.map(make_influx_point))
    retries = Retry(connect=5, read=3, redirect=5)
    write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=10_000, flush_interval=10_000))
    write_api.write("cosmoz", record=data, write_precision=WritePrecision.S, retries=retries)
    write_api.close()


def do_diagnostic_data(site_no):
    result = influx1_client.query(
        """\
    SELECT "time", site_no, flag, detector_no, * FROM "diagnostic_data"
    WHERE site_no=$s;""",
        bind_params={"s": str(site_no)},
    )

    def make_influx_point(p: dict):
        # on diagnostic_data, flag is int
        # phs is b64 string
        return (
            Point("diagnostic_data")
            .tag("site_no", p['site_no'])
            .tag("flag", int(p.get('flag', 0)))
            .tag("detector_no", int(p['detector_no']))
            .time(parse_datetime(p['time']), write_precision=WritePrecision.S)
            .field("payload_timestamp", math.floor(parse_datetime(p['payload_timestamp']).timestamp()))
            .field("total_neutron_counts", int(p['total_neutron_counts']))
            .field("mins_of_accum", int(p['mins_of_accum']))
            .field("low_temp", int(p['low_temp']))
            .field("high_temp", int(p['high_temp']))
            .field("avg_temp", int(p['avg_temp']))
            .field("low_rel_hum", int(p['low_rel_hum']))
            .field("high_rel_hum", int(p['high_rel_hum']))
            .field("avg_rel_hum", int(p['avg_rel_hum']))
            .field("npm_index", int(p['npm_index']))
            .field("phs_9", int(p['phs_9']))
            .field("phs", str(p['phs']))
        )

    data = rx.from_iterable(result.get_points()).pipe(ops.map(make_influx_point))
    write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=10_000, flush_interval=10_000))
    write_api.write("cosmoz", record=data, write_precision=WritePrecision.S, retries=retries)
    write_api.close()


def do_status_data(site_no):
    result = influx1_client.query(
        """\
    SELECT "time", site_no, flag, * FROM "status_data"
    WHERE site_no=$s;""",
        bind_params={"s": str(site_no)},
    )

    def make_influx_point(p: dict):
        # on status_data, flag is int
        # yes, fw_version really is a float (wtf)
        # adc_scale_parameters is a json string (floats_
        # d_io_setup is a json string (integers)
        # npm_info is a json string (dicts)
        return (
            Point("status_data")
            .tag("site_no", p['site_no'])
            .time(parse_datetime(p['time']), write_precision=WritePrecision.S)
            .field("payload_timestamp", math.floor(parse_datetime(p['payload_timestamp']).timestamp()))
            .field("fw_version", float(p['fw_version']))
            .field("record_period", int(p['record_period']))
            .field("data_select_string", str(p['data_select_string']))
            .field("adc_scale_parameters", str(p['adc_scale_parameters']))
            .field("p1cal", float(p['p1cal']))
            .field("d_io_setup", str(p['d_io_setup']))
            .field("npm_info", str(p['npm_info']))
            .field("npm_diag_interval", int(p['npm_diag_interval']))
            .field("relays", int(p['relays']))
            .field("power_conservation", int(p['power_conservation']))
            .field("pwr_5v_on", bool(p['pwr_5v_on']))
            .field("pwr_12v_on", bool(p['pwr_12v_on']))
            .field("conserve_5v_pwr", bool(p['conserve_5v_pwr']))
            .field("conserve_12v_pwr", bool(p['conserve_12v_pwr']))
            .field("conserve_npm_pwr", bool(p['conserve_npm_pwr']))
            .field("battery_threshold_on", bool(p['battery_threshold_on']))
            .field("daily_pwr_conserve_on", bool(p['daily_pwr_conserve_on']))
            .field("low_battery_threshold", float(p['low_battery_threshold']))
            .field("charged_battery_threshold", float(p['charged_battery_threshold']))
            .field("daily_power_up", str(p['daily_power_up']))
            .field("daily_power_down", str(p['daily_power_down']))
            .field("vmc_mux_channels", int(p['vmc_mux_channels']))
            .field("vmc_mux_sat_channels", int(p['vmc_mux_sat_channels']))
            .field("satellite_data_select_string", str(p['satellite_data_select_string']))
            .field("records_per_satellite_transmission", int(p['records_per_satellite_transmission']))
            .field("max_rapid_satellite_signal_queries", int(p['max_rapid_satellite_signal_queries']))
            .field("satellite_retry_period", int(p['satellite_retry_period']))
            .field("satellite_transmission_persistence", int(p['satellite_transmission_persistence']))
            .field("update_time_via_satellite", int(p['update_time_via_satellite']))
            .field("power_down_history", str(p['power_down_history']))
            .field("boot_up_type_history", str(p['boot_up_type_history']))
            .field("boot_up_history", str(p['boot_up_history']))
            .field("remote_password", str(p['remote_password']))
            .field("use_satellite_modem", bool(p['use_satellite_modem']))
            .field("use_cell_modem", bool(p['use_cell_modem']))
            .field("use_vwc_mux", bool(p['use_vwc_mux']))
            .field("ini_updated_via_satellite", bool(p['ini_updated_via_satellite']))
            .field("command_received_via_satellite", bool(p['command_received_via_satellite']))
            .field("satellite_password_error", bool(p['satellite_password_error']))
            .field("rtc_battery_low", bool(p['rtc_battery_low']))
            .field("npm_history_satellite_send", bool(p['npm_history_satellite_send']))
            .field("satellite_rf_pickup_reduction", bool(p['satellite_rf_pickup_reduction']))
            .field("power_sdi_12", int(p['power_sdi_12']))
            .field("npm_serial_numbers", str(p['npm_serial_numbers']))
            .field("npm_number_in_bins", str(p['npm_number_in_bins']))
            .field("npm_vibe_modes", str(p['npm_vibe_modes']))
            .field(
                "number_of_satellite_commands_received_since_last_ini",
                int(p['number_of_satellite_commands_received_since_last_ini']),
            )
            .field("satellite_data_2_period", int(p['satellite_data_2_period']))
            .field("records_per_satellite_data_2_transmission", int(p['records_per_satellite_data_2_transmission']))
            .field("data_2_select_string", str(p['data_2_select_string']))
            .field("p3cal", float(p['p3cal']))
            .field("p4cal", float(p['p4cal']))
            .field("p5cal", float(p['p5cal']))
            .field("p6cal", float(p['p6cal']))
        )

    data = rx.from_iterable(result.get_points()).pipe(ops.map(make_influx_point))
    write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=10_000, flush_interval=10_000))
    write_api.write("cosmoz", record=data, write_precision=WritePrecision.S, retries=retries)
    write_api.close()


def do_silo(site_no=1):
    result = influx1_client.query(
        """\
SELECT "time", site_no, * FROM "silo_data"
WHERE site_no=$s;""",
        bind_params={"s": str(site_no)},
    )

    def make_influx_point(p: dict):
        # on silo_data, flag is int
        return (
            Point("silo_data")
            .tag("site_no", p['site_no'])
            .time(p['time'], write_precision=WritePrecision.S)
            .field("t_max", float(p['t_max']))
            .field("smx", float(p['smx']))
            .field("t_min", float(p['t_min']))
            .field("smn", float(p['smn']))
            .field("rain", float(p['rain']))
            .field("srn", float(p['srn']))
            .field("evap", float(p['evap']))
            .field("sev", float(p['sev']))
            .field("radn", float(p['radn']))
            .field("ssl", float(p['ssl']))
            .field("vp", float(p['vp']))
            .field("svp", float(p['svp']))
            .field("rh_max_t", float(p['rh_max_t']))
            .field("rh_min_t", float(p['rh_min_t']))
            .field("average_temperature", float(p['average_temperature']))
            .field("average_humidity", float(p['average_humidity']))
        )

    data = rx.from_iterable(result.get_points()).pipe(ops.map(make_influx_point))
    write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=10_000, flush_interval=10_000))
    write_api.write("cosmoz", record=data, write_precision=WritePrecision.S, retries=retries)
    write_api.close()


def do_intensity(site_no=1):
    result = influx1_client.query(
        """\
SELECT "time", site_no, intensity, bad_data_flag FROM "intensity"
WHERE site_no=$s;""",
        bind_params={"s": str(site_no)},
    )

    def make_influx_point(p: dict):
        # on level1, flag is int
        return (
            Point("intensity")
            .tag("site_no", p['site_no'])
            .tag("bad_data_flag", int(p['bad_data_flag']))
            .time(p['time'], write_precision=WritePrecision.S)
            .field("intensity", float(p['intensity']))
        )

    data = rx.from_iterable(result.get_points()).pipe(ops.map(make_influx_point))
    write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=10_000, flush_interval=10_000))
    write_api.write("cosmoz", record=data, write_precision=WritePrecision.S, retries=retries)
    write_api.close()


def do_level_1(site_no=1):
    result = influx1_client.query(
        """\
SELECT "time", site_no, "count", pressure1, internal_temperature, internal_humidity, battery, tube_temperature, tube_humidity, rain, vwc1, vwc2, vwc3, pressure2, external_temperature, external_humidity, flag as level1_flag
FROM "level1"
WHERE site_no=$s;""",
        bind_params={"s": str(site_no)},
    )

    def make_influx_point(p: dict):
        # on level1, flag is int
        return (
            Point("level1")
            .tag("site_no", p['site_no'])
            .tag("flag", int(p['level1_flag']))
            .time(p['time'], write_precision=WritePrecision.S)
            .field("count", int(p['count']))
            .field("pressure1", float(p['pressure1']))
            .field("internal_temperature", float(p['internal_temperature']))
            .field("internal_humidity", float(p['internal_humidity']))
            .field("battery", float(p['battery']))
            .field("tube_temperature", float(p['tube_temperature']))
            .field("tube_humidity", float(p['tube_humidity']))
            .field("rain", float(p['rain']))
            .field("vwc1", float(p['vwc1']))
            .field("vwc2", float(p['vwc2']))
            .field("vwc3", float(p['vwc3']))
            .field("pressure2", float(p['pressure2']))
            .field("external_temperature", float(p['external_temperature']))
            .field("external_humidity", float(p['external_humidity']))
        )

    data = rx.from_iterable(result.get_points()).pipe(ops.map(make_influx_point))
    write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=10_000, flush_interval=10_000))
    write_api.write("cosmoz", record=data, write_precision=WritePrecision.S, retries=retries)
    write_api.close()


def do_level_raw(site_no=1):
    result = influx1_client.query(
        """\
SELECT "time", site_no, "count", pressure1, internal_temperature, internal_humidity, battery, tube_temperature, tube_humidity, rain, vwc1, vwc2, vwc3, pressure2, external_temperature, external_humidity, flag as raw_flag
FROM "raw_values"
WHERE site_no=$s;""",
        bind_params={"s": str(site_no)},
    )

    def make_influx_point(p: dict):
        # raw_values has flag as string, all other tables have flag is int
        return (
            Point("raw_values")
            .tag("site_no", p['site_no'])
            .tag("flag", str(p['raw_flag']))
            .time(p['time'], write_precision=WritePrecision.S)
            .field("count", int(p['count']))
            .field("pressure1", float(p['pressure1']))
            .field("internal_temperature", float(p['internal_temperature']))
            .field("internal_humidity", float(p['internal_humidity']))
            .field("battery", float(p['battery']))
            .field("tube_temperature", float(p['tube_temperature']))
            .field("tube_humidity", float(p['tube_humidity']))
            .field("rain", float(p['rain']))
            .field("vwc1", float(p['vwc1']))
            .field("vwc2", float(p['vwc2']))
            .field("vwc3", float(p['vwc3']))
            .field("pressure2", float(p['pressure2']))
            .field("external_temperature", float(p['external_temperature']))
            .field("external_humidity", float(p['external_humidity']))
        )

    data = rx.from_iterable(result.get_points()).pipe(ops.map(make_influx_point))
    write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=10_000, flush_interval=10_000))
    write_api.write("cosmoz", record=data, write_precision=WritePrecision.S, retries=retries)
    write_api.close()


def main():
    mongo_client = MongoClient(mongodb_config['DB_HOST'], int(mongodb_config['DB_PORT']))  # 27017
    mdb = getattr(mongo_client, mongodb_config['DB_NAME'])
    all_stations_docs = mdb.all_stations
    all_stations = all_stations_docs.find({}, {'site_no': 1})
    mongo_client.close()
    all_stations = list(all_stations)  # This turns a mongo cursor into a python list
    for s in all_stations:
        do_diagnostic_data(s['site_no'])
        do_power_data(s['site_no'])
        # do_status_data(s['site_no'])
        # do_silo(s['site_no'])
        # do_intensity(s['site_no'])
        # do_level_raw(s['site_no'])
        # do_level_1(s['site_no'])
    # do_level_2()
    # do_level_3()
    # do_level_4()


if __name__ == "__main__":
    main()
