# -*- coding: utf-8 -*-
#
import sys

from os import getenv


try:
    from .utils import do_load_dotenv
except ImportError:
    from utils import do_load_dotenv

do_load_dotenv()

module = sys.modules[__name__]
defaults = module.defaults = {
    "DB_HOST": "timeseriesdb",
    "DB_PORT": 8086,
    "DB_BUCKET": "cosmoz",
    "DB_USERNAME": "username",
    "DB_ORGANIZATION": "tern-landscapes",
    "DB_PASSWORD": "password",
    "DB_TOKEN": "mytokenrwsecret==",
}
config = module.config = dict()

config['DB_HOST'] = getenv("INFLUX2_HOST", None)
config['DB_PORT'] = getenv("INFLUX2_PORT", None)
config['DB_BUCKET'] = getenv("INFLUX2_BUCKET", None)
config['DB_ORGANIZATION'] = getenv("INFLUX2_ORGANIZATION", None)
config['DB_USERNAME'] = getenv("INFLUX2_USERNAME", None)
config['DB_PASSWORD'] = getenv("INFLUX2_PASSWORD", None)
config['DB_TOKEN'] = getenv("INFLUX2_TOKEN", None)

for k, v in defaults.items():
    if k not in config or config[k] is None:
        config[k] = v

DB_HOST = config['DB_HOST']
DB_PORT = int(config['DB_PORT'])
DB_BUCKET = config['DB_BUCKET']
DB_USERNAME = config['DB_USERNAME']
DB_ORGANIZATION = config['DB_ORGANIZATION']
DB_PASSWORD = config['DB_PASSWORD']
DB_TOKEN = config['DB_TOKEN']
