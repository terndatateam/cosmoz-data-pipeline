#!/bin/python3
# -*- coding: utf-8 -*-
"""
Copyright 2022 CSIRO Land and Water

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import WriteOptions
from urllib3 import Retry

from ._influx2_db_config import config as influx2_config


retries = Retry(connect=5, read=3, redirect=5)

scheme = "https" if int(influx2_config['DB_PORT']) in (443, 8443, 9443, 10443) else "http"
influx2_client = InfluxDBClient(
    "{}://{}:{}".format(scheme, influx2_config['DB_HOST'], int(influx2_config['DB_PORT'])),
    influx2_config['DB_TOKEN'],
    org=influx2_config["DB_ORGANIZATION"],
    timeout=30000,
    retries=retries,
)


def do_measurement(measurement, old_site_no, new_site_no, delete_old=False):
    read_api = influx2_client.query_api()
    raw_res = read_api.query(
        """\
    from(bucket: "cosmoz")
      |> range(start: 1970-01-01)
      |> filter(fn: (r) => r._measurement == m)
      |> filter(fn: (r) => r.site_no == s)
    """,
        params={"s": str(old_site_no), "m": str(measurement)},
    )
    try:
        raw_tables_iter = iter(raw_res)
        table = next(raw_tables_iter)
    except StopIteration:
        print("No data found for old-site-number: {}".format(old_site_no), flush=True)
        del read_api  # close read_api
        return
    number_moved = 0
    while table:
        write_api = influx2_client.write_api(write_options=WriteOptions(batch_size=10_000, flush_interval=10_000))
        for r in table.records:
            p = Point(measurement_name=measurement)
            p = (
                p.time(r.get_time(), write_precision=WritePrecision.S)
                .tag("site_no", new_site_no)
                .field(r.get_field(), r.get_value())
            )
            for k, v in r.values.items():
                if k in ("result", "table", "_start", "_stop", "_time", "_field", "_value", "_measurement"):
                    continue
                elif k in ("site_no",):
                    continue
                # This must be a tag, not a field, because fields are denormalized in this query
                p = p.tag(k, v)
            write_api.write("cosmoz", record=p, write_precision=WritePrecision.S, retries=retries)
            number_moved += 1
        write_api.close()
        try:
            table = next(raw_tables_iter)
        except StopIteration:
            break
    if delete_old and number_moved > 0:
        del_me = influx2_client.delete_api()
        del_me.delete(
            "1970-01-01T00:00:00Z",
            "2199-12-31T23:59:59Z",
            predicate='_measurement="{}" AND site_no="{}"'.format(measurement, str(old_site_no)),
            bucket="cosmoz",
        )


def main():
    # do_measurement("raw_values", "25", "26", True)
    # do_measurement("raw_values", "24", "25", True)
    # do_measurement("level1", "25", "26", True)
    # do_measurement("level1", "24", "25", True)
    # do_measurement("level2", "25", "26", True)
    # do_measurement("level2", "24", "25", True)
    # do_measurement("level3", "25", "26", True)
    # do_measurement("level3", "24", "25", True)
    # do_measurement("level4", "25", "26", True)
    # do_measurement("level4", "24", "25", True)
    # do_measurement("diagnostic_data", "25", "26", True)
    # do_measurement("diagnostic_data", "24", "25", True)
    do_measurement("intensity", "25", "26", True)
    do_measurement("intensity", "24", "25", True)
    do_measurement("silo_data", "25", "26", True)
    do_measurement("silo_data", "24", "25", True)
    do_measurement("power_cycle_data", "25", "26", True)
    do_measurement("power_cycle_data", "24", "25", True)
    do_measurement("status_data", "25", "26", True)
    do_measurement("status_data", "24", "25", True)


if __name__ == "__main__":
    main()
