from pathlib import Path
from typing import Optional, Dict, List

from ciso8601 import parse_datetime
from functools import lru_cache
from collections import defaultdict
from urllib import request
from urllib.parse import urljoin
from datetime import datetime
import json
from ._config import PUBLIC_COSMOZ_API


@lru_cache()
def get_all_cosmoz_stations_from_public_api():
    if PUBLIC_COSMOZ_API is None:
        raise RuntimeError("No Public Cosmoz API endpoint set. Cannot use it.")
    stations_endpoint = urljoin(PUBLIC_COSMOZ_API, "stations")
    headers = {
        "Accept": "application/json",
    }
    r = request.Request(stations_endpoint, headers=headers)
    with request.urlopen(r) as response:
        if 200 <= response.code < 300:
            text = response.read()
            try:
                payload = json.loads(text)
            except Exception:
                raise RuntimeError("Cannot decode stations JSON text!")
        else:
            raise RuntimeError("Cannot read stations response from Public API endpoint")
    try:
        stations = payload["stations"]
    except LookupError:
        raise RuntimeError("Stations not found in Public API response.")
    all_stations_dict = {}
    for s in stations:
        if "site_no" not in s:
            continue
        all_stations_dict[s["site_no"]] = s
    return all_stations_dict

def get_cosmoz_station_from_public_api(station_no: int):
    if PUBLIC_COSMOZ_API is None:
        raise RuntimeError("No Public Cosmoz API endpoint set. Cannot use it.")
    stations_endpoint = urljoin(PUBLIC_COSMOZ_API, "stations")
    this_station_endpoint = stations_endpoint + "/" + str(station_no)
    headers = {
        "Accept": "application/json",
    }
    r = request.Request(this_station_endpoint, headers=headers)
    with request.urlopen(r) as response:
        if 200 <= response.code < 300:
            text = response.read()
            try:
                payload = json.loads(text)
            except Exception:
                raise RuntimeError("Cannot decode stations JSON text!")
        else:
            raise RuntimeError("Cannot read stations response from Public API endpoint")
    try:
        station_info = payload["station"]
    except LookupError:
        raise RuntimeError("Station not found in Public API response.")
    return station_info

@lru_cache()
def get_imei_to_station_map_from_public_api():
    stations = get_all_cosmoz_stations_from_public_api()
    stations_by_imei: Dict[str, List[dict]] = defaultdict(list)
    for site_no, station in stations.items():
        stations_by_imei[str(station.get("imei", 0)).lower()].append(station)
    return stations_by_imei


@lru_cache()
def get_store_folder_to_station_map_from_public_api(folder_attr: str):
    stations = get_all_cosmoz_stations_from_public_api()
    stations_by_folder_name: Dict[str, List[dict]] = defaultdict(list)
    for site_no, station in stations.items():
        stations_by_folder_name[str(station.get(folder_attr, "")).lower()].append(station)
    return stations_by_folder_name


@lru_cache()
def get_site_data_from_imei_from_public_api(imei: str, at_date=None):
    stations_by_imei = get_imei_to_station_map_from_public_api()
    found_sites = stations_by_imei.get(str(imei).lower(), None)
    if found_sites is None or len(found_sites) < 1:
        raise LookupError()
    elif len(found_sites) < 2:
        found_site = found_sites[0]
    elif at_date is None:
        # get the one most recently installed
        found_sites.sort(key=lambda x: x.get('installation_date', None), reverse=True)
        found_site = found_sites[0]
    else:
        # get the one most recently installed but before sent_date
        found_sites.sort(key=lambda x: x.get('installation_date', None), reverse=True)
        found_site = found_sites[-1]
        if isinstance(at_date, datetime):
            at_date = at_date.date()
        for f in found_sites:
            i_date = f.get('installation_date', None)
            if i_date is None:
                continue
            if isinstance(i_date, str):
                i_date = parse_datetime(i_date)
            if isinstance(i_date, datetime):
                i_date = i_date.date()
            if i_date <= at_date:
                found_site = f
                break
    if not found_site:
        raise LookupError()
    return found_site

