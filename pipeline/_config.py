# -*- coding: utf-8 -*-
#
"""Main config file for the app, place in here things that don't go in the other config files"""
import sys
from os import getenv
from .utils import do_load_dotenv

undefined = object()
do_load_dotenv()

module = sys.modules[__name__]
# These are config defaults
defaults = module.defaults = {
    "USE_COSMOZ_PUBLIC_API": "",  # Empty string is closest we can get to "False"
    "PUBLIC_COSMOZ_API": "https://landscapes-cosmoz-api.tern.org.au/rest/",
    "NMDB_OPERATING_MODE": "SIMPLIFIED",
}
config = module.config = dict()
# get configs from env vars
config['USE_COSMOZ_PUBLIC_API'] = getenv("USE_COSMOZ_PUBLIC_API", undefined)
config['PUBLIC_COSMOZ_API'] = getenv("PUBLIC_COSMOZ_API", undefined)
config['NMDB_OPERATING_MODE'] = getenv("NMDB_OPERATING_MODE", undefined)

# fill defaults into config if env vars are not provided
for k, v in defaults.items():
    if k not in config or config[k] is undefined:
        if v is None:
            raise RuntimeError(f"No default value available for config key \"{k}\"")
        config[k] = v

# write config values to module top-level, for easier import into other files
USE_COSMOZ_PUBLIC_API = config["USE_COSMOZ_PUBLIC_API"]
PUBLIC_COSMOZ_API = config['PUBLIC_COSMOZ_API']
NMDB_OPERATING_MODE = config['NMDB_OPERATING_MODE']
