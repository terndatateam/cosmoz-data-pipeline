import datetime
from multiprocessing.pool import ThreadPool

from pymongo import MongoClient
from urllib3 import Retry
from influxdb_client import InfluxDBClient as InfluxDB2Client
from influxdb_client import Point, WritePrecision
from influxdb_client.client.write_api import WriteOptions
from urllib3.exceptions import ReadTimeoutError
import reactivex as rx
from reactivex import operators as ops
import time
try:
    from ._influx2_db_config import config as influx2_config
    from ._mongo_db_config import config as mongodb_config
    from .cosmoz_api import get_all_cosmoz_stations_from_public_api
    from ._config import USE_COSMOZ_PUBLIC_API
except ImportError:
    from _influx2_db_config import config as influx2_config
    from _mongo_db_config import config as mongodb_config
    from cosmoz_api import get_all_cosmoz_stations_from_public_api
    from _config import USE_COSMOZ_PUBLIC_API


retries = Retry(connect=5, read=10, redirect=10)
scheme = "https" if int(influx2_config['DB_PORT']) in (443, 8443, 9443, 10443) else "http"

def get_mongo_client():
    return MongoClient(mongodb_config['DB_HOST'], int(mongodb_config['DB_PORT']))

def get_influx_client():
     return InfluxDB2Client(
        "{}://{}:{}".format(scheme, influx2_config['DB_HOST'], int(influx2_config['DB_PORT'])),
        influx2_config['DB_TOKEN'],
        org=influx2_config["DB_ORGANIZATION"],
        timeout=30000,
        retries=retries,
    )

def get_all_stations():
    if USE_COSMOZ_PUBLIC_API:
        all_stations_map = get_all_cosmoz_stations_from_public_api()
        all_stations = all_stations_map.values()
    else:
        mongo_client = get_mongo_client()
        mdb = getattr(mongo_client, mongodb_config['DB_NAME'])
        all_stations_docs = mdb.all_stations
        all_stations = all_stations_docs.find({}, {'site_no': 1})
        mongo_client.close()
        all_stations = list(all_stations)  # This turns the mongo cursor into a python list
    return all_stations

def _make_daily_point(site_no: int, p, db_measurement: str):
    this_datetime = p['time']
    point = Point(f"daily_{db_measurement}")\
            .tag("site_no", str(site_no))\
            .time(this_datetime, write_precision=WritePrecision.S)
    for k,v in p.items():
        if k in ("time", "_time", "site_no"):
            continue
        point = point.field(k, v)
    return point

def push_to_influx(aggregated_obs:list, site_number=1, processing_level=4, influx_client=None, dryrun=True):
    USE_MULTITHREAD = False
    if influx_client is None:
        influx_client = get_influx_client()
    write_api = influx_client.write_api(write_options=WriteOptions(batch_size=1_000, flush_interval=5_000))
    if processing_level < 1:
        db_measurement = "raw_values"
    else:
        db_measurement = f"level{str(processing_level)}"
    if USE_MULTITHREAD:
        tp = ThreadPool(16)
        try:
            data_observable = rx.from_(aggregated_obs)
            tasks_array = []
            tasks_pipe = data_observable.pipe(
                ops.map(
                    lambda p: tp.apply_async(_make_daily_point, (site_number, p, db_measurement))
                )
            )
            _ = tasks_pipe.subscribe(
                on_next=lambda t: tasks_array.append(t),
                on_error=lambda ex: print(f'Unexpected error: {ex}', flush=True),
            )
            data_pipe = rx.from_(tasks_array).pipe(ops.map(lambda t: t.get()), ops.filter(lambda r: r is not None))
            if not dryrun:
                write_api.write("cosmoz", write_precision=WritePrecision.S, record=data_pipe)
        finally:
            tp.close()
            write_api.close()
    else:
        try:
            data_observable = rx.from_(aggregated_obs)
            data_pipe = data_observable.pipe(
                ops.map(lambda p: _make_daily_point(site_number, p, db_measurement)),
                ops.filter(lambda r: r is not None),
            )
            if not dryrun:
                write_api.write("cosmoz", write_precision=WritePrecision.S, record=data_pipe)
        finally:
            write_api.close()


def get_24h_aggregate_influx(site_number, startdate: datetime.datetime = None, enddate: datetime.datetime = None, processing_level=4, influx_client=None):
    # this just gets min, max, mean, and count for soil_moisture, and effective_depth. Rain is not included.
    # It always aggregates starting at midnight on startdate, and ends at beginning of enddate (doesnt include enddate)
    if influx_client is None:
        influx_client = get_influx_client()
    site_number = int(site_number)
    aggregate = "24h"
    if startdate is None:
        # set since to 1970-01-01T00:00:00Z
        start_datetime_string = "1970-01-01T00:00:00.000Z"
    else:
        if isinstance(startdate, datetime.datetime):
            startdate = startdate.replace(hour=0, minute=0, second=0, microsecond=0)
            start_datetime_string = startdate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
        elif isinstance(startdate, str):
            start_datetime_string = startdate
        else:
            raise RuntimeError()

    if enddate is None:
        enddate = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)

    if isinstance(enddate, datetime.datetime):
        # reset it to midnight at the start of this day, so this day is not included!
        enddate = enddate.replace(hour=0, minute=0, second=0, microsecond=0)
        end_datetime_string = enddate.strftime("%Y-%m-%dT%H:%M:%S.000Z")
    elif isinstance(enddate, str):
        end_datetime_string = enddate
    else:
        raise RuntimeError()

    if processing_level < 1:
        db_measurement = "raw_values"
        property_filter = ["count"]
    else:
        db_measurement = f"level{str(processing_level)}"
        if processing_level == 1:
            property_filter = ["count"]
        elif processing_level == 2:
            property_filter = ["corr_count"]
        elif processing_level == 3:
            property_filter = ["soil_moist", "effective_depth"]
        elif processing_level == 4:
            property_filter = ["soil_moist_filtered", "depth_filtered"]
        else:
            property_filter = ["*"]

    get_all = ""
    if property_filter and len(property_filter) > 0:
        if '*' in property_filter:
            fields_string = get_all
        else:
            fields = []
            for p in property_filter:
                fields.append("r[\"_field\"] == \"{}\"".format(p))
            fields_joined = " or ".join(fields)
            fields_string = "|> filter(fn: (r) => {})".format(fields_joined)
    else:
        fields_string = get_all


    # We can only aggregate over time, using values where flag == 0
    if processing_level in (0, 1, 2, 3):
        drop_flag = "|> filter(fn: (r) => r[\"flag\"] == \"0\")"
    else:
        drop_flag = ""
    # Note, switch this back to aggregateWindow once the native version of min and max are merged
    # query_str = """\
    #     import "experimental"
    #     data = from(bucket: "cosmoz")
    #         |> range(start: {start:s}, stop: {stop:s})
    #         |> filter(fn: (r) => r["_measurement"] == m)
    #         |> filter(fn: (r) => r["site_no"] == s)
    #         {drop_flag:s}
    #         {fields:s}
    #         aggregate = (tables=<-, name, fn) => tables
    #             |> aggregateWindow(fn: fn, every: {ag:s}, timeSrc: "_start", createEmpty: false)
    #             |> map(fn: (r) => ({{r with _metricType: name}}))
    #             |> experimental.group(columns: ["_metricType"], mode: "extend")
    #         datamin = data |> aggregate(name: "min", fn: min)
    #         datamax = data |> aggregate(name: "max", fn: max)
    #         datamean = data |> aggregate(name: "mean", fn: mean)
    #         datacount = data |> aggregate(name: "count", fn: count)
    #
    #         union(tables: [datamin, datamax, datamean, datacount])
    #         |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
    #         |> drop(columns: ["table", "_measurement", "_start", "_stop", "site_no"])""".format(
    #     start=start_datetime_string, stop=end_datetime_string,
    #     fields=fields_string, ag=aggregate,
    #     drop_flag=drop_flag
    # )
    query_str = """\
    import "experimental"
    data = from(bucket: "cosmoz")
        |> range(start: {start:s}, stop: {stop:s})
        |> filter(fn: (r) => r["_measurement"] == m)
        |> filter(fn: (r) => r["site_no"] == s)
        {drop_flag:s}
        {fields:s}
    data_w = data |> window(every: {ag:s})
    data_mean = data_w
        |> experimental.mean()
        |> duplicate(column: "_start", as: "_time")
        |> map(fn: (r) => ({{r with _metricType: "mean"}}))
        |> experimental.group(columns: ["_metricType"], mode: "extend")
    data_min = data_w
        |> experimental.min()
        |> duplicate(column: "_start", as: "_time")
        |> map(fn: (r) => ({{r with _metricType: "min"}}))
        |> experimental.group(columns: ["_metricType"], mode: "extend")
    data_max = data_w
        |> experimental.max()
        |> duplicate(column: "_start", as: "_time")
        |> map(fn: (r) => ({{r with _metricType: "max"}}))
        |> experimental.group(columns: ["_metricType"], mode: "extend")
    data_count = data_w
        |> experimental.count()
        |> duplicate(column: "_start", as: "_time")
        |> map(fn: (r) => ({{r with _metricType: "count"}}))
        |> experimental.group(columns: ["_metricType"], mode: "extend")
    union(tables: [data_min, data_max, data_mean, data_count])
        |> pivot(rowKey: ["_time"], columnKey: ["_field"], valueColumn: "_value")
        |> drop(columns: ["table", "_measurement", "_start", "_stop", "site_no"])""".format(
        start=start_datetime_string, stop=end_datetime_string,
        fields=fields_string, ag=aggregate,
        drop_flag=drop_flag
    )

    query_api = influx_client.query_api()
    try:
        query_res = query_api.query(query_str, params={
            "m": db_measurement, "s": str(site_number)
        })
    except ReadTimeoutError as e:
        print(f"Read timeout from InfluxDB backend when getting observations for site {site_number}", flush=True)
        raise

    count = 0
    observations = {}
    if query_res:
        tables = {}
        for t in iter(query_res):
            try:
                first = next(iter(t))
            except StopIteration:
                continue
            else:
                metric = first["_metricType"]
                tables[metric] = t
        try:
            mins_table = tables['min']
            maxs_table = tables['max']
            means_table = tables['mean']
            counts_table = tables['count']
        except LookupError:
            raise RuntimeError("Cannot get min, max, mean, count responses from InfluxDB")
        for (max_row, min_row, mean_row, count_row) in zip(maxs_table, mins_table, means_table, counts_table):
            this_time = mean_row.values.pop("_time")
            observation = {
                "time": this_time
            }
            for pfx, row_vals in (("max_",max_row.values), ("min_",min_row.values), ("mean_",mean_row.values), ("count_",count_row.values)):
                for (k,v) in row_vals.items():
                    if k in ("_time", "time", "result", "table", "_metricType", "site_no", "flag"):
                        continue
                    observation[pfx+str(k)] = v
            observations[this_time] = observation
            count = count + 1

    sorted_obs = [observations[d] for d in sorted(observations.keys())]


    return sorted_obs

def main():
    all_stations = get_all_stations()
    for s in all_stations:
        site_no = s['site_no']
        for processing_level in (0, 1, 2, 3, 4):
            time.sleep(1)
            sorted_obs = get_24h_aggregate_influx(site_number=site_no, processing_level=processing_level)
            push_to_influx(sorted_obs, site_number=site_no, processing_level=processing_level, dryrun=False)

if __name__ == "__main__":
    main()
