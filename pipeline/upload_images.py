import sys
import asyncio
import glob
from pathlib import Path
from motor.motor_asyncio import AsyncIOMotorClient, AsyncIOMotorGridFSBucket
from ._mongo_db_config import config as mongodb_config



async def upload_pics(patterns, delete=False):
    client = AsyncIOMotorClient(mongodb_config['DB_HOST'], int(mongodb_config['DB_PORT']))  # 27017
    db = getattr(client, mongodb_config['DB_NAME'])

    all_stations_collection = db.all_stations
    bucket = AsyncIOMotorGridFSBucket(db, bucket_name='fs')
    here = Path(".").absolute()
    files = []
    for p in patterns:
        files.extend(here.glob(p))
    for f in files:
        file_name = f.name
        cursor = bucket.find({"filename": file_name}, no_cursor_timeout=True)
        skip_file = False
        async for existing_file in cursor:
            print("Exists!", file_name)
            if delete:
                await bucket.delete(existing_file._id)
                print("Deleted existing.")
            else:
                print("Skipping", f)
                skip_file = True
                break
        if skip_file:
            continue
        with open(f, 'rb') as fd:
            try:
                file_id = await bucket.upload_from_stream(file_name, fd)
            except Exception as e:
                print(e)
                continue
            else:
                print("Uploaded", file_name, file_id)





def main():
    patterns = ["./*.jpg", "./*.jpeg", "./*.png"]
    return asyncio.get_event_loop().run_until_complete(upload_pics(patterns, delete=True))

if __name__ == "__main__":
    print("Cannot run this module directly. Launch it from the helper script in the parent folder.")
    sys.exit(1)
